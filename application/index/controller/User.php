<?php
namespace app\index\controller;
use think\Controller;
use think\Session;

class User extends Comm
{
	//显示
	public function user()
	{
        indexLogin();
        $id = Session::get('id');
        $rs=db('user')->where(['u_id'=>$id])->find();
		return view('user',['rs'=>$rs]);
	}
	//修改
	public function xiu()
	{
        indexLogin();
        $id = Session::get('id');
        if($_POST)
		{
			$u_name=input('u_name');
			$u_pwd=input('u_pwd');
			$u_rpwd=input('u_rpwd');
			if ($u_name==''){
			    $this->error("用户名不能为空！","xiu");
            }else{
			    $name_length = mb_strlen($u_name,'utf-8');
			    if($name_length<3||$name_length>8){
			        $this->error("请输入3——8位用户名！");
                }
            }
            if($u_rpwd==''){
                $this->error("原密码不能为空","xiu");
            }else{
                $ssss=db("user")->where(["u_id"=>$id])->find();
                $rpwd = $ssss["u_pwd"];
                if($rpwd!=md5($u_rpwd)){
                    $this->error("原码输入不正确！",'xiu');
                }
            }
			if($u_pwd==''){
			    $this->error("密码不能为空","xiu");
            }else{
			    $pwd_length = mb_strlen($u_pwd,"utf-8");
			    if($pwd_length<6){
			        $this->error("请输入至少6位数密码","xiu");
                }
            }
			$data["u_name"] = $u_name;
			$data["u_pwd"] = md5($u_pwd);
			//echo md5($u_pwd);
			$rs=db('user')->where(['u_id'=>$id])
				->update($data);
			if($rs) {
                echo $this->success('修改成功', 'user');
            } else {
			    echo $this->error('修改失败','xiu');
			}
		}
        $rs=db('user')->where('u_id',$id)->find();
        return view('xiu',['rs'=>$rs]);
	}
	//充值
    public function chong(){
        $id = Session::get('id');
        indexLogin();
	    if($_POST){
	        $danhao = input("danhao");
	        $price = input("price");
	        //var_dump($danhao);
            if ($price==""){
                $this->error("充值金额不能为空！","chong");
            }
            if ($danhao==""){
                $this->error("订单号不能为空！","chong");
            }else{
                $rrr = db("ch_order")->where(["ch_acount"=>$danhao])->find();
                if($rrr!=""){
                    $this->error("此订单已充值使用，请勿重复提交！");
                }
            }
            if($danhao) {
                $data["ch_acount"] = $danhao;
                $data["u_id"] = $id;
                $data["ch_time"] = time();
                $data["ch_price"]=$price;
                $insert = db("ch_order")->insert($data);
                if($insert){
                    $this->success("提交成功","chong");
                }else{
                    $this->error("提交失败","chong");
                }
            }
        }
		$this->assign('shuoming',db('price')->where(['p_id'=>10])->value('p_shuoming'));
        return view('chong');
    }
    public function or_order(){
        indexLogin();
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND ro_url LIKE '%" . input("key") . "%'";
            }
        }
        $c = $where;
        //var_dump($c);
        $rs = db("price_order")
			->where($where)
			->field('po_name,po_val,from_unixtime(po_time,"%Y-%m-%d %H:%i:%s") as po_time,po_price_before,po_price_after')
			->order("po_time DESC")
			->paginate(15);
        $length = db("price_order")->where($where)->count('po_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('or_order', ['rs' => $rs, 'page' => $page]);
    }
    public function ch_order(){
        indexLogin();
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND ch_acount LIKE '%" . input("key") . "%'";
            }
        }
        $rs = db("ch_order")
			->where($where)
			->field('
					ch_acount,
					ch_price,ch_type,
					from_unixtime(ch_time,"%Y-%m-%d %H:%i:%s") as ch_time,
					from_unixtime(ch_time_end,"%Y-%m-%d %H:%i:%s") as ch_time_end,
					case ch_type when 0 then "处理中" when 1 then "已到账" when 2 then "已驳回" end as type
					')
			->order("ch_time DESC")
			->paginate(15);
        $length = db("ch_order")->where($where)->count('ch_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('ch_order', ['rs' => $rs, 'page' => $page]);
    }
}
?>