<?php

namespace app\index\controller;

use think\Controller;
use think\Session;

class qita extends Controller
{
    public function qita_order(){
        indexLogin();
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND qita_name LIKE '%" . input("key") . "%'";
            }
        }
        $rs = db("qita_order")
            ->where($where)
            ->field('
				qita_leixing,
				qita_name,
				qita_number_plan,
				qita_moshi,
				qita_type,
				qita_id,
				qita_price,
				from_unixtime(qita_time_end,"%Y-%m-%d %H:%i:%s") as qita_time_end,
				from_unixtime(qita_time_start,"%Y-%m-%d %H:%i:%s") as qita_time_start
				')
            ->order("qita_id DESC")
            ->paginate(15);
        $length = db("qita_order")->where($where)->count('qita_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('qita_order', ['rs' => $rs, 'page' => $page]);
    }
    public function qita()
    {
        indexLogin();
        $id = Session::get('id');
        if ($_POST) {
            $qita_name = input("qita_name");
            //var_dump($qita_name);
            $qita_number_plan = input("qita_number_plan");
            $p_id = input("p_id");
            $b_id = input("b_id");
            $price_all = db('price')->where(["wx_id " => 0, "b_id" => $b_id,"p_id" => $p_id])->find();
            $qita_moshi =$price_all["p_name"];
            $data["qita_moshi"] = $qita_moshi;
            //var_dump($price_all);
            $price_one1 = $price_all["p_price_one"];
            $danwei1 = $price_all["p_number"];
            $num = $qita_number_plan / $danwei1 * $price_one1;
            //var_dump($num);
            echo "<br />";
            $qita_price = number_format($num, 2);
            $qita_price_f = str_replace(",", "", $qita_price);
            $data["qita_name"] = $qita_name;
            $data["qita_number_plan"] = $qita_number_plan;
            $data["qita_price"] = $qita_price_f;
            $data['u_id'] = $id;
            $data['p_id'] = $p_id;
            $data['b_id'] = $b_id;
            $time_start = time();
            $data['qita_time_start']=$time_start;
            $u_money = db("user")->where(["u_id" => $id])->find()["u_money"];
            if ($u_money < $qita_price_f) {
                $this->error("余额不足，请充值！", "/index/user/chong");
            } else {
                $u_money_after = ($u_money - $qita_price_f);
                $sive = db("user")->where(["u_id" => $id])->update(["u_money" => $u_money_after]);
                if (!$sive) {
                    $this->error("提交失败", 'add');
                } else {
                    $po_name_ls = db("business")->where(["b_id"=>$b_id])->find();
                    $po_name = $po_name_ls["b_name"];
                    $data["qita_leixing"] = $po_name;
                    $price_order["po_price_before"] = $u_money . "元";
                    $price_order["po_price_after"] = $u_money_after . "元";
                    $price_order["po_name"] = $po_name;
                    $price_order["po_val"] = "-" . $qita_price_f . "元";
                    $price_order["u_id"] = $id;
                    $price_order["po_time"] = time();
                    $save_price_order = db("price_order")->insert($price_order);
                    if (!$save_price_order) {
                        $this->error("提交失败", 'add');
                    } else {
                        $inset = db("qita_order")->insert($data);
                        if ($inset) {
                            echo $this->success('提交成功', 'qita_order');
                        } else {
                            echo $this->error('提交失败', 'qita');
                        }
                    }
                }
            }
        }
        $where = array("wx_id"=>0,"b_id"=>1,"p_power"=>1);
        $s = input("bid");
        //var_dump($s);
        $business = db("business")->select();
        if (input("bid") != ''&&input("p_id1")!='') {
            // $where = "wx_id = 0 AND b_id =" . input("bid") ."AND p_power = 1" ;
            $where = array("wx_id"=>0,"b_id"=>input("bid"),"p_power"=>1);
            //moshi
            $system_max = db("price")->where(["wx_id" => 0,"b_id"=>input("bid"),"p_power"=>1])
                ->select();
            $price_list = db("price")->where($where)->select();
            $number_system=db("price")->where(["p_id"=>input("p_id1"),"p_power"=>1])->find();
            $number_max = $number_system["p_max"];
            $number_min = $number_system["p_min"];
            //var_dump($where);
            if (input("key") != ''&&input("b_id")!='') {
                $where = array("wx_id"=>0,"b_id"=>input("b_id"),"p_id"=>input("key"),"p_power"=>1);
                $number_system=db("price")->where($where)->find();
                $number_max = $number_system["p_max"];
                $number_min = $number_system["p_min"];
                //var_dump($where);
            }
            $price_ll = db('price')->where($where)->find();
            //var_dump($price_ll);echo ("<br />");
            $price_one = $price_ll["p_price_one"];
            //echo $price_one;die;
            if($price_one==""){
                $this->error("亲，很抱歉，目前暂停接单，请稍后再试！","qita_order");
            }
            $danwei = $price_ll["p_number"];
            $shuoming = $price_ll['p_shuoming'];
            $price = $price_one."元/".$danwei.$price_ll["p_number_danwei"];
            return view('qita',['number_max' => $number_max,'number_min' => $number_min,'price'=>$price,'price_one'=>$price_one,'danwei'=>$danwei,'price_list'=>$price_list,'shuoming'=>$shuoming,'business'=>$business]);
        } else {
            $system_max = db("price")->where(["wx_id" => 0,"b_id"=>1,"p_power"=>1])
                ->find();
            $number_max = $system_max["p_max"];
            $number_min = $system_max["p_min"];
            if (input("b_id")!=''){
                //  $where = "wx_id = 0 AND b_id =" . input("b_id") ."AND p_power = 1";
                $where = array("wx_id"=>0,"b_id"=>input("bid"),"p_power"=>1);
            }
            $price_list = db("price")->where($where)->select();
            //var_dump($where);
            if (input("key") != ''&&input("b_id")!='') {
                $where = array("wx_id"=>0,"b_id"=>input("b_id"),"p_id"=>input("key"),"p_power"=>1);
                $number_system=db("price")->where($where)->find();
                $number_max = $number_system["p_max"];
                $number_min = $number_system["p_min"];
                //var_dump($where);
            }
            $price_ll = db('price')->where($where)->find();
            //var_dump($price_ll);echo ("<br />");
            $price_one = $price_ll["p_price_one"];
            if($price_one==""){
                $this->error("亲，很抱歉，目前暂停接单，请稍后再试！","qita_order");
            }
            $danwei = $price_ll["p_number"];
            $shuoming = $price_ll['p_shuoming'];
            $price = $price_one."元/".$danwei.$price_ll["p_number_danwei"];
            return view('qita',['number_max' => $number_max,'number_min' => $number_min,'price'=>$price,'price_one'=>$price_one,'danwei'=>$danwei,'price_list'=>$price_list,'shuoming'=>$shuoming,'business'=>$business]);
        }
    }
    public function qita_chexiao(){
        indexLogin();
        $id = Session::get('id');
        if(request()->isPost()){
            $qita_id = input("key");
            $ls = db("qita_order")->where(["qita_id"=>$qita_id])->find();
            if($ls["qita_type"]!=0){
                $this->error("撤销失败,请刷新页面","/index/qita/qita_order");
            }
            $b_id = $ls["b_id"];
            $bb = db("business")->where(["b_id"=>$b_id])->find();
            $po_name = $bb["b_name"];
            $qita_price = $ls["qita_price"];
            $user_list = db("user")->where(["u_id"=>$id])->find();
            $u_money = $user_list["u_money"];
            $u_money_after = ($u_money+$qita_price);
            $update_user = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
            if(!$update_user){
                echo $this->error("撤销失败！");
            } else{
                $price_order["po_price_before"] = $u_money."元";
                $price_order["po_price_after"] = $u_money_after."元";
                $price_order["po_name"] = $po_name."退款";
                $price_order["po_val"] = "+".$qita_price."元";
                $price_order["u_id"] = $id;
                $price_order["po_time"] = time();
                $save_price_order = db("price_order")->insert($price_order);
                if(!$save_price_order){
                    echo $this->error("撤销失败！");
                } else{
                    $rs = db("qita_order")->where(["qita_id"=>$qita_id])->update(["qita_type"=>2,"qita_price"=>0]);
                    if(!$rs) {
                        echo $this->error('撤销失败！');
                    } else {
                        $this->success("order");
                    }
                }
            }
        }
    }
}