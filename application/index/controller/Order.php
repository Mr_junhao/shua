<?php

namespace app\index\controller;

use think\Controller;
use think\Session;

class order extends Controller
{
    public function order()
    {
        indexLogin();
        //curl 爬取网页 获取文章标题与阅读量  价格比例
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND ro_url LIKE '%" . input("key") . "%'";
            }
        }
        $c = $where;
        //var_dump($c);
        $rs = db("read_order")
            ->where($where)
            ->field('
				ro_name,
				ro_number_start,
				ro_number,
				ro_type,
				ro_number_plan,
				ro_number_start,
				from_unixtime(ro_time_start,"%Y-%m-%d %H:%i:%s") as ro_time_start,
				ro_price,
				ro_moshi,
				ro_id,
				from_unixtime(ro_time_end,"%Y-%m-%d %H:%i:%s") as ro_time_end,
				ro_url
				')
            ->order("ro_time_start DESC")
            ->paginate(15);
        //$rs = db("read_order")->where($where)->order("ro_time_start DESC")->select();
        $length = db("read_order")->where($where)->count('ro_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('order', ['rs' => $rs,'page'=>$page,'length'=>$length]);
    }

    public function add()
    {
        indexLogin();
        $id = Session::get('id');
        if ($_POST) {
            $ro_url = input("ro_url");
            $cha_where["ro_url"] =array('eq',$ro_url);
            $cha_where["ro_type"]=array('lt',2);
            $cha = db("read_order")->where($cha_where)->select();
            foreach ($cha as $v){
                if ($v!=""){
                    $this->error("此链接已有任务！正在占用");
                }
            }
            $ro_number_plan = input("ro_number_plan");
            if($ro_number_plan == ""){
                $this->error("阅读量不能为空！");
            }
            $where_22["ro_url"] =array('eq',$ro_url);
            $where_22["ro_type"] = array('neq',2);
            //获取 URL 为用户输入URL 状态为进行中，已完成的所有订单
            $ch2 = db("read_order")->where($where_22)->select();
            // echo"<pre />";print_r($ch2);
            //获取订单条数
            $length_ch23 = count($ch2);
            // var_dump($ch2);echo "<br />";
            $sum = 0;
            //循环 每一条订单将计划量取出 相加 获取单条连接已下单量
            for ($i=0;$i<$length_ch23;$i++){

                // echo $ch2[$i]["ro_number_plan"]; echo "<br />";
                $sum+=$ch2[$i]["ro_number_plan"];

            }
            //判断电条连接总量是否在10万的限制内
            if(($sum+$ro_number_plan)>1000000){
                //剩余数量
                // echo $sum;die;
                $liang = (1000000-$sum);
                //var_dump($sum+$ro_number_plan);
                $this->error("单个连接总阅读量超过1000000,剩余".$liang);
            }

            /*$ro_price = input("ro_price");
            var_dump($ro_price);*/
            $p_id = input("p_id");
            $price_all = db('price')->where(["wx_id "=>1,"p_id"=>$p_id])->find();
            $ro_moshi =$price_all["p_name"];
            $data["ro_moshi"] = $ro_moshi;
            $price_one1 = $price_all["p_price_one"];
            $danwei1 = $price_all["p_number"];
            $num = $ro_number_plan/$danwei1*$price_one1;
            $ro_price = number_format($num,2);
            $ro_price_f = str_replace(",","",$ro_price);
            $api_id = db("price")->where(['p_id'=>$p_id])->value("api_id");
            $data["api_id"]=$api_id;
            //echo $price_one1;
            //var_dump( Session::get("price_one_session"));die;
           // var_dump($price_one1);die;
            $price_after = db('price')->where(["wx_id"=>1,"p_power"=>1,"p_name"=> $data["ro_moshi"]])->find();
            //var_dump($price_after);die;
            //$price_one_after = $price_after["p_price_one"];
            if ($price_after==""){
                    $this->error("亲，很抱歉，此模式暂停接单，请稍后再试！","order");
            }
            if ( Session::get("price_one_session")==""|| Session::get("price_one_session")==null){
                $session_price_one = $price_one1;
            }else {
                $session_price_one = Session::get("price_one_session");
            }
            if ($session_price_one!=$price_one1){

                    $this->error("亲，很抱歉，价格已修改，请重试！");
            }
            if ($ro_url) {
                $data2 = $this->DLip();
                $UserAgent = 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380';
                $ch = curl_init();    //创建一个新的CURL资源
                curl_setopt ($ch, CURLOPT_PROXY, $data2);
                curl_setopt($ch, CURLOPT_URL, $ro_url);    //设置URL和相应的选项
                curl_setopt($ch, CURLOPT_HEADER, 0);  //0表示不输出Header，1表示输出
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_ENCODING, '');    //设置编码格式，为空表示支持所有格式的编码
                curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                $data1 = curl_exec($ch);
                //var_dump($data2);die;
                curl_close($ch);
                //echo $res;die;
                //$data1 = file_get_contents($ro_url);
                //var_dump($data1) ;die;
                preg_match_all('|<h2 class="rich_media_title" id="activity-name">(.*?)<\/h2>|is', $data1, $title1);
                preg_match_all('|var biz = ""\|\|"(.*?)";|is', $data1, $biz1);
                preg_match_all('|var sn = "" \|\| ""\|\| "(.*?)";|is', $data1, $sn1);
                preg_match_all('|var mid = "" \|\| ""\|\| "(.*?)";|is', $data1, $mid1);
                preg_match_all('|var idx = "" \|\| "" \|\| "(.*?)";|is', $data1, $idx1);
                //var_dump($title1);die;
                if(empty($title1[1])){
                    $this->error("请检查连接是否可用","add");
                }
                $title = trim($title1[1][0]);
                $biz = $biz1[1][0];
                $sn = $sn1[1][0];
                $mid = $mid1[1][0];
                $idx = $idx1[1][0];
                $data['ro_url']=$ro_url;
                $data['ro_name']=$title;
                $data['ro_price']=$ro_price_f;
                $data['ro_number_plan']=$ro_number_plan;
                $data['ro_number']=0;
                $data['u_id']=$id;
                $data['p_id']=$p_id;
                $data['biz']=$biz;
                $data['mid']=$mid;
                $data['idx']=$idx;
                $data['sn']=$sn;
                $time_start = time();
                $data['ro_time_start']=$time_start;
                $u_money = db("user")->where(["u_id"=>$id])->find()["u_money"];
                // var_dump($u_money);
                if($u_money<$ro_price_f){
                    $this->error("余额不足，请充值！","/index/user/chong");
                }else{
                    $u_money_after = ($u_money-$ro_price_f);
                    $sive = db("user")->where(["u_id"=>$id])->update(array("u_money"=>$u_money_after));
                    if(!$sive){
                        $this->error("提交失败",'add');
                    }else{
                        $price_order["po_price_before"] = $u_money."元";
                        $price_order["po_price_after"] = $u_money_after."元";
                        $price_order["po_name"] = "阅读量";
                        $price_order["po_val"] = "-".$ro_price_f."元";
                        $price_order["u_id"] = $id;
                        $price_order["po_time"] = $time_start;
                        $save_price_order = db("price_order")->insert($price_order);
                        if(!$save_price_order){
                            $this->error("提交失败",'add');
                        }else{
                            $inset = db("read_order")->insert($data);
                            if($inset){
                                echo $this->success('提交成功', 'order');
                            }else{
                                db("price_order")->where($price_order)->delete();
                                echo $this->error('提交失败', 'add');
                            }
                        }
                    }
                }
            }
        }
        $where = "wx_id = 1 AND p_power = 1";
        /*if (request()->isPost()) {*/
        $s=input("key");
        //var_dump($s);
        if (input("key") != '') {
            $where .= " AND p_id = " . input("key") ;
            //var_dump($where);
        }
        //}
        //var_dump($where);
        $system = db("system")
            ->select();
        $number_max_list = db("price")->where($where)->find();
        $number_max = $number_max_list["p_max"];
        $number_min_list = db("price")->where($where)->find();
        $number_min = $number_min_list["p_min"];
        $price_list = db("price")-> where(['wx_id'=>1,"p_power"=>1])->select();
        //var_dump($price_list);die;

        $price_ll = db('price')->where($where)->find();
        $price_one = $price_ll["p_price_one"];
        Session::set("price_one_session",$price_one);
        if($price_one==""){
            //Session::set("price_one_stop",$price_one);
            $this->error("亲，很抱歉，目前暂停接单，请稍后再试！","order");
        }
        //var_dump($price_list);echo ("<br />");
        $danwei = $price_ll["p_number"];
        $shuoming = $price_ll['p_shuoming'];
        $price = $price_one."元/".$danwei.$price_ll["p_number_danwei"];
        return view('add',['number_max' => $number_max,'number_min' => $number_min,'price'=>$price,'price_one'=>$price_one,'danwei'=>$danwei,'price_list'=>$price_list,'shuoming'=>$shuoming]);
    }
    public function chexiao(){
        indexLogin();
        $id = Session::get('id');
        if(request()->isPost()){
            $ro_id = input("key");
            $ls = db("read_order")->where(["ro_id"=>$ro_id])->find();
            if($ls["ro_type"]!=0){
                $this->error("撤销失败,请刷新页面","/index/order/order");
            }
            $ro_price = $ls["ro_price"];
            $user_list = db("user")->where(["u_id"=>$id])->find();
            $u_money = $user_list["u_money"];
            $u_money_after = ($u_money+$ro_price);
            $update_user = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
            if(!$update_user){
                $this->error("撤销失败");
            } else{
                $price_order["po_price_before"] = $u_money."元";
                $price_order["po_price_after"] = $u_money_after."元";
                $price_order["po_name"] = "阅读量";
                $price_order["po_val"] = "+".$ro_price."元";
                $price_order["u_id"] = $id;
                $price_order["po_time"] = time();
                $save_price_order = db("price_order")->insert($price_order);
                if(!$save_price_order){
                    $this->error("撤销失败！");
                } else{
                    $rs = db("read_order")->where(["ro_id"=>$ro_id])->update(["ro_type"=>2,"ro_price"=>0]);
                    if(!$rs) {
                        $this->error('撤销失败');
                    } else {
                        $this->success("order");
                    }
                }
            }
        }
        //return view('order');

    }
    public function zan_order()
    {
        indexLogin();
        //curl 爬取网页 获取文章标题与阅读量  价格比例
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND zo_url LIKE '%" . input("key") . "%'";
            }
        }
        $rs = db("zan_order")
            ->where($where)
            ->field('
				zo_number_start,
				zo_number_plan,
				zo_number,
				zo_type,
				zo_moshi,
				zo_name,
				zo_id,
				from_unixtime(zo_time_start,"%Y-%m-%d %H:%i:%s") as zo_time_start,
				zo_price,
				from_unixtime(zo_time_end,"%Y-%m-%d %H:%i:%s") as zo_time_end,
				zo_url
				')
            ->order("zo_time_start DESC")
            ->paginate(15);
        $length = db("zan_order")->where($where)->count('zo_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('zan_order', ['rs' => $rs, 'page' => $page]);
    }
    public function zan_add()
    {
        indexLogin();
        $id = Session::get('id');
        if ($_POST) {
            $zo_url = input("zo_url");
            if($zo_url==""){
                $this->error("文章链接不能为空");
            }
            $cha_where["zo_url"] =array('eq',$zo_url);
            $cha_where["zo_type"]=array('lt',2);
            $cha = db("zan_order")->where($cha_where)->select();
            foreach ($cha as $v){
                if ($v!=""){
                    $this->error("此链接已有任务！正在占用");
                }
            }
            $zo_number_plan = input("zo_number_plan");
            if($zo_number_plan ==""){
                $this->error("点赞量不能为空");
            }
            $where_22["zo_url"] =array('eq',$zo_url);
            $where_22["zo_type"] = array('neq',2);
            $ch2 = db("zan_order")->where($where_22)->select();
            $length_ch2 = count($ch2);
            // var_dump($ch2);echo "<br />";
            $sum = 0;
            for ($i=0;$i<$length_ch2;$i++){
                // foreach ($ch2 as $v){
                $sum+=$ch2[$i]["zo_number_plan"];
                // }
            }
            if(($sum+$zo_number_plan)>=50000){
                $liang = (50000-$sum);
                $this->error("单个连接总阅读量超过50000,剩余".$liang);
            }
            /*$zo_price = input("zo_price");
            var_dump($zo_price);*/
            $p_id = input("p_id");
            $price_all = db('price')->where(["wx_id "=>2,"p_id"=>$p_id])->find();
            $zo_moshi =$price_all["p_name"];
            $data["zo_moshi"] = $zo_moshi;
            $price_one1 = $price_all["p_price_one"];
            $danwei1 = $price_all["p_number"];
            $num = $zo_number_plan/$danwei1*$price_one1;
            $zo_price = number_format($num,2);
            $zo_price_f = str_replace(",","",$zo_price);
            $price_after = db('price')->where(["wx_id"=>2,"p_power"=>1,"p_name"=>$zo_moshi])->find();
            if ($price_after==""){
                $this->error("亲，很抱歉，此模式暂停接单，请稍后再试！","zan_order");
            }
            if ( Session::get("price_one_session_zan")==""|| Session::get("price_one_session_zan")==null){
                $session_price_one = $price_one1;
            }else {
                $session_price_one = Session::get("price_one_session_zan");
            }
            if ($session_price_one!=$price_one1){

                $this->error("亲，很抱歉，价格已修改，请重试！");
            }
            if ($zo_url) {
                $data2 = $this->DLip();
                $UserAgent = 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380';
                $ch = curl_init();    //创建一个新的CURL资源
                curl_setopt ($ch, CURLOPT_PROXY, $data2);
                curl_setopt($ch, CURLOPT_URL, $zo_url);    //设置URL和相应的选项
                curl_setopt($ch, CURLOPT_HEADER, 0);  //0表示不输出Header，1表示输出
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_ENCODING, '');    //设置编码格式，为空表示支持所有格式的编码
                curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                $data1 = curl_exec($ch);
                curl_close($ch);    //关闭cURL资源，并释放系统资源
                preg_match_all('|<h2 class="rich_media_title" id="activity-name">(.*?)<\/h2>|is', $data1, $title1);
                preg_match_all('|var biz = ""\|\|"(.*?)";|is', $data1, $biz1);
                preg_match_all('|var sn = "" \|\| ""\|\| "(.*?)";|is', $data1, $sn1);
                preg_match_all('|var mid = "" \|\| ""\|\| "(.*?)";|is', $data1, $mid1);
                preg_match_all('|var idx = "" \|\| "" \|\| "(.*?)";|is', $data1, $idx1);
                //var_dump()
                if(empty($title1[1])){
                    $this->error("请检查连接是否可用","zan_add");
                }
                $title = trim($title1[1][0]);
                $biz = $biz1[1][0];
                $sn = $sn1[1][0];
                $mid = $mid1[1][0];
                $idx = $idx1[1][0];
                //echo($title);
                /* $url3 = "https://mp.weixin.qq.com/s?__biz=$biz&mid=$mid&idx=$idx&sn=$sn";
                 $urll = urlencode($url3);
                 //echo $urll;
                 $url = "http://api.bizapi.cn:521/bizapi/getnumber?username=121706713&password=475286&url=$urll";
                 $UserAgent = 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380';
                 $curl = curl_init();    //创建一个新的CURL资源
                 curl_setopt($curl, CURLOPT_URL, $url);    //设置URL和相应的选项
                 curl_setopt($curl, CURLOPT_HEADER, 0);  //0表示不输出Header，1表示输出
                 curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                 curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                 curl_setopt($curl, CURLOPT_ENCODING, '');    //设置编码格式，为空表示支持所有格式的编码
                 curl_setopt($curl, CURLOPT_USERAGENT, $UserAgent);
                 curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
                 $data2 = curl_exec($curl);
                 curl_close($curl);    //关闭cURL资源，并释放系统资源
                 //echo $data2;die;
                 //$result = (json_decode((json_decode($data2))->result)->appmsgstat)->like_num;
                   $bb= json_decode($data2);
                   //var_dump($bb);die;
                $aa=$bb ->result;
                 $cc = json_decode($aa);
                 $dd = $cc->appmsgstat;
                /// var_dump($dd);die;
                $like_num=$dd->like_num;*/
                //var_dump($like_num);die;
                /* $data["zo_number_start"]=$like_num;
                 $data["zo_number"]=$like_num;*/
                $data['zo_url']=$zo_url;
                $data['zo_name']=$title;
                $data['zo_price']=$zo_price_f;
                $data['zo_number_plan']=$zo_number_plan;
                $data['zo_number']=0;
                $data['u_id']=$id;
                $data['p_id']=$p_id;
                $data['biz']=$biz;
                $data['mid']=$mid;
                $data['idx']=$idx;
                $data['sn']=$sn;
                $time_start = time();
                $data['zo_time_start']=$time_start;
                $api_id = db("price")->where(['p_id'=>$p_id])->value("api_id");
                $data["api_id"]=$api_id;
                //var_dump($api_id);die();
                /* if($api_id==1){
                     $post["url"]=$urll;
                     $post['number']=$zo_number_plan;
                     $post["speed_model"] = 0;
                     $post['from']="wechat";
                     $post["token"]="5e2c920912e0b71b08638370cebc287b";
                     $api_url = "http://wechat.goudaoke.com/api/home//read/create";
                     $res = $this->post_curls($api_url,$post);
                     //var_dump($res);die;
                     $result = json_decode($res);
                     $message = $result->message;
                     $orderid= $message->orderid;
                     //var_dump($orderid);die;
                     $data['orderid']=$orderid;
                     $data['zo_type']=1;
                 }
                 if($api_id==2){
                     $post["url"]=$urll;
                     $post["number"]=$zo_number_plan;
                 }*/
                //var_dump($data);die;
                $u_money = db("user")->where(["u_id"=>$id])->find()["u_money"];
                if($u_money<$zo_price_f){
                    $this->error("余额不足，请充值！","/index/user/chong");
                }else{
                    $u_money_after = ($u_money-$zo_price_f);
                    $sive = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
                    $price_order["po_price_before"] = $u_money."元";
                    $price_order["po_price_after"] = $u_money_after."元";
                    $price_order["po_name"] = "点赞量";
                    $price_order["po_val"] = "-".$zo_price_f."元";
                    $price_order["u_id"] = $id;
                    $price_order["po_time"] = $time_start;
                    $save_price_order = db("price_order")->insert($price_order);
                    //var_dump($save_price_order);
                    if(!$save_price_order){
                        $this->error("提交失败2",'add');
                    }else{
                        $inset = db("zan_order")->insert($data);
                        if($inset){
                            echo $this->success('提交成功', 'zan_order');
                        }else{
                            echo $this->error('提交失败', 'zan_add');
                        }
                    }
                }
            }
        }
        $where = "wx_id = 2 AND p_power = 1";
        /*if (request()->isPost()) {*/
        $s=input("key");
        //var_dump($s);die;
        if (input("key") != '') {
            $where .= " AND p_id = " . input("key") ;

        }
        //}
        //die;
        //var_dump($where);die;
        $system = db("system")
            ->select();
        $number_max_list = db("price")->where($where)->find();
        $number_max = $number_max_list["p_max"];
        $number_min_list = db("price")->where($where)->find();
        $number_min = $number_min_list["p_min"];
        $price_list = db("price")-> where(['wx_id'=>2,"p_power"=>1])->select();
        $price_ll = db('price')->where($where)->find();
        $price_one = $price_ll["p_price_one"];
        Session::set("price_one_session_zan",$price_one);
        if($price_one==""){
            $this->error("亲，很抱歉，目前暂停接单，请稍后再试！","zan_order");
        }
        //var_dump($price_list);echo ("<br />");
        $danwei = $price_ll["p_number"];
        $shuoming = $price_ll['p_shuoming'];
        $price = $price_one."元/".$danwei.$price_ll["p_number_danwei"];
        return view('zan_add',['number_max' => $number_max,'number_min' => $number_min,'price'=>$price,'price_one'=>$price_one,'danwei'=>$danwei,'price_list'=>$price_list,'shuoming'=>$shuoming]);
    }
    public function zan_chexiao(){
        indexLogin();
        $id = Session::get('id');
        if(request()->isPost()){
            $zo_id = input("key");
            $ls = db("zan_order")->where(["zo_id"=>$zo_id])->find();
            if($ls["zo_type"]!=0){
                $this->error("撤销失败,请刷新页面","/index/order/zan_order");
            }
            $zo_price = $ls["zo_price"];
            $user_list = db("user")->where(["u_id"=>$id])->find();
            $u_money = $user_list["u_money"];
            $u_money_after = ($u_money+$zo_price);
            $update_user = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
            if(!$update_user){
                echo $this->error("撤销失败！");
            } else{
                $price_order["po_price_before"] = $u_money."元";
                $price_order["po_price_after"] = $u_money_after."元";
                $price_order["po_name"] = "点赞量";
                $price_order["po_val"] = "+".$zo_price."元";
                $price_order["u_id"] = $id;
                $price_order["po_time"] = time();
                $save_price_order = db("price_order")->insert($price_order);
                if(!$save_price_order){
                    echo $this->error("撤销失败！");
                } else{
                    $rs = db("zan_order")->where(["zo_id"=>$zo_id])->update(["zo_type"=>2,"zo_price"=>0]);
                    if(!$rs) {
                        echo $this->error('撤销失败！');
                    } else {
                        $this->success("order");
                    }
                }
            }
        }
    }
    public function fen_order(){
        indexLogin();
        $id = Session::get('id');
        $where = "u_id = $id";
        if (request()->isPost()) {
            $s=input("key");
            // var_dump($s);
            if (input("key") != '') {
                $where .= " AND fen_name LIKE '%" . input("key") . "%'";
            }
        }
        $rs = db("fen_order")
            ->where($where)
            ->field('
				fen_name,
				fen_number_plan,
				fen_moshi,
				schedule,
				fen_type,
				from_unixtime(fen_time_start,"%Y-%m-%d %H:%i:%s") as fen_time_start,
				fen_price,
				from_unixtime(fen_time_end,"%Y-%m-%d %H:%i:%s") as fen_time_end,
				fen_id
				')
            ->order("fen_id DESC")
            ->paginate(15);
        $length = db("fen_order")->where($where)->count('fen_id');
        $page = $rs->render();
        $this->assign('length', $length);
        return view('fen_order', ['rs' => $rs, 'page' => $page]);
    }
    public function fen_chexiao(){
        indexLogin();
        $id = Session::get('id');
        if(request()->isPost()){
            $fen_id = input("key");
            $ls = db("fen_order")->where(["fen_id"=>$fen_id])->find();
            if($ls["fen_type"]!=0){
                $this->error("撤销失败,请刷新页面","/index/order/fen_order");
            }
            $fen_price = $ls["fen_price"];
            $user_list = db("user")->where(["u_id"=>$id])->find();
            $u_money = $user_list["u_money"];
            $u_money_after = ($u_money+$fen_price);
            $update_user = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
            if(!$update_user){
                echo $this->error("撤销失败！");
            } else{
                $price_order["po_price_before"] = $u_money."元";
                $price_order["po_price_after"] = $u_money_after."元";
                $price_order["po_name"] = "点赞量";
                $price_order["po_val"] = "+".$fen_price."元";
                $price_order["u_id"] = $id;
                $price_order["po_time"] = time();
                $save_price_order = db("price_order")->insert($price_order);
                if(!$save_price_order){
                    echo $this->error("撤销失败！");
                } else{
                    $rs = db("fen_order")->where(["fen_id"=>$fen_id])->update(["fen_type"=>2,"fen_price"=>0]);
                    if(!$rs) {
                        echo $this->error('撤销失败！');
                    } else {
                        $this->success("order");
                    }
                }
            }
        }
    }
    public function fensi(){
        indexLogin();
        $id = Session::get('id');
        if ($_POST) {
            $fen_name = input("fen_name");
            $cha_where["fen_name"] =array('eq',$fen_name);
            $cha_where["fen_type"]=array('lt',2);
            $cha = db("fen_order")->where($cha_where)->select();
            foreach ($cha as $v){
                if ($v!=""){
                    $this->error("此链接已有任务！正在占用");
                }
            }
            $fen_number_plan = input("fen_number_plan");
            $p_id = input("p_id");
            //var_dump($fen_name);echo "<br />";
            $price_all = db('price')->where(["wx_id "=>3,"p_id"=>$p_id])->find();
            $fen_moshi =$price_all["p_name"];
            $data["fen_moshi"] = $fen_moshi;
            //var_dump($price_all);
            $price_one1 = $price_all["p_price_one"];
            $danwei1 = $price_all["p_number"];
            $num = $fen_number_plan/$danwei1*$price_one1;
            $price_after = db('price')->where(["wx_id"=>3,"p_power"=>1,"p_name"=>$fen_moshi])->find();
            if ($price_after==""){
                $this->error("亲，很抱歉，此模式暂停接单，请稍后再试！","fen_order");
            }
            if ( Session::get("price_one_session_fen")==""|| Session::get("price_one_session_fen")==null){
                $session_price_one = $price_one1;
            }else {
                $session_price_one = Session::get("price_one_session_fen");
            }
            if ($session_price_one!=$price_one1){

                $this->error("亲，很抱歉，价格已修改，请重试！");
            }
            //var_dump($num);echo"<br />";
            $fen_price = number_format($num,2);
            $fen_price_f = str_replace(",","",$fen_price);
            $data["fen_name"] = $fen_name;
            $data["fen_number_plan"] = $fen_number_plan;
            $data["fen_price"] = $fen_price_f;
            $data['u_id'] = $id;
            $data['p_id']=$p_id;
            $api_id = db("price")->where(['p_id'=>$p_id])->value("api_id");
            $data["api_id"]=$api_id;
            $time_start = time();
            $data['fen_time_start']=$time_start;
            $u_money = db("user")->where(["u_id"=>$id])->find()["u_money"];
            if($u_money < $fen_price_f){
                $this->error("余额不足，请充值！","/index/user/chong");
            }else{
                $u_money_after = ($u_money-$fen_price_f);
                $sive = db("user")->where(["u_id"=>$id])->update(["u_money"=>$u_money_after]);
                if(!$sive){
                    $this->error("提交失败",'add');
                }else{
                    $price_order["po_price_before"] = $u_money."元";
                    $price_order["po_price_after"] = $u_money_after."元";
                    $price_order["po_name"] = "粉丝量";
                    $price_order["po_val"] = "-".$fen_price_f."元";
                    $price_order["u_id"] = $id;
                    $price_order["po_time"] = time();
                    $save_price_order = db("price_order")->insert($price_order);
                    if(!$save_price_order){
                        $this->error("提交失败",'add');
                    }else{
                        $inset = db("fen_order")->insert($data);
                        if($inset){
                            echo $this->success('提交成功', 'fen_order');
                        }else{
                            echo $this->error('提交失败', 'fensi');
                        }
                    }
                }
            }
        }
        $where = "wx_id = 3 AND p_power =1";
        $s=input("key");
        if (input("key") != '') {
            $where .= " AND p_id = " . input("key") ;
            //var_dump($where);
        }
        $number_max_list = db("price")->where($where)->find();
        $number_max = $number_max_list["p_max"];
        $number_min_list = db("price")->where($where)->find();
        $number_min = $number_min_list["p_min"];
        $price_list = db("price")->where(['wx_id'=>3,"p_power"=>1])->select();
        $price_ll = db('price')->where($where)->find();
        //var_dump($price_ll);echo ("<br />");
        $price_one = $price_ll["p_price_one"];
        Session::set("price_one_session_fen",$price_one);
        if($price_one==""){
            $this->error("亲，很抱歉，目前暂停接单，请稍后再试！","fen_order");
        }
        $danwei = $price_ll["p_number"];
        $shuoming = $price_ll['p_shuoming'];
        $price = $price_one."元/".$danwei.$price_ll["p_number_danwei"];
        return view('fensi',['number_max' => $number_max,'number_min' => $number_min,'price'=>$price,'price_one'=>$price_one,'danwei'=>$danwei,'price_list'=>$price_list,'shuoming'=>$shuoming]);
    }
    public function post_curls($url, $post)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $res = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $res; // 返回数据，json格式

    }
    /*public function whileIP($ip){
        $url="http://h.zbzok.com/Users-whiteIpAddNew.html?appid=XX&appkey=XX&whiteip=$ip";
        $UserAgent = 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380';
        $curl = curl_init();    //创建一个新的CURL资源
        curl_setopt($curl, CURLOPT_URL, $url);    //设置URL和相应的选项
        curl_setopt($curl, CURLOPT_HEADER, 0);  //0表示不输出Header，1表示输出
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_ENCODING, '');    //设置编码格式，为空表示支持所有格式的编码
        curl_setopt($curl, CURLOPT_USERAGENT, $UserAgent);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $data2 = curl_exec($curl);
        curl_close($curl);    //关闭cURL资源，并释放系统资源
    }*/
    public function DLip(){
        /* 第二家代理*/
        //$url = "http://www.66ip.cn/nmtq.php?getnum=1&isp=0&anonymoustype=0&start=&ports=&export=&ipaddress=&area=0&proxytype=2&api=66ip";

        /**/
         //$url = "http://ip.11jsq.com/index.php/api/entry?method=proxyServer.generate_api_url&packid=1&fa=0&fetch_key=&qty=1&time=1&pro=&city=&port=1&format=txt&ss=1&css=&dt=1&specialTxt=3&specialJson=";

        /* 第一家代理*/
        $url="http://t.11jsq.com/index.php/api/entry?method=proxyServer.generate_api_url&packid=7&fa=1&fetch_key=&qty=1&time=1&pro=&city=&port=1&format=txt&ss=1&css=&dt=0&specialTxt=3&specialJson=";
        $UserAgent = 'Mozilla/5.0 (Linux; U; Android 4.1.2; zh-cn; GT-I9300 Build/JZO54K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30 MicroMessenger/5.2.380';
        $curl = curl_init();    //创建一个新的CURL资源
        curl_setopt($curl, CURLOPT_URL, $url);    //设置URL和相应的选项
        curl_setopt($curl, CURLOPT_HEADER, 0);  //0表示不输出Header，1表示输出
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_ENCODING, '');    //设置编码格式，为空表示支持所有格式的编码
        curl_setopt($curl, CURLOPT_USERAGENT, $UserAgent);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $data2 = curl_exec($curl);
        curl_close($curl);    //关闭cURL资源，并释放系统资源
        return $data2;
    }
}