<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
use think\Session;
function indexLogin()
{
    //parent::_initialize();
    $u_phone=Session::has('u_phone');
	$url=url('index/index/login');
	if(empty($u_phone))
	{
		header('location:'.$url);
		die;
	}
}
function adminLogin()
{
    //parent::_initialize();
    $name=Session::has('name');
    $url=url('shua_admin/index/login');
    if(empty($name))
    {
        header('location:'.$url);
        die;
    }
}
