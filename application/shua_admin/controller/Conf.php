<?php

namespace app\shua_admin\controller;

use think\Controller;
use think\Session;

class Conf extends Controller
{
	public function conf()
	{
		$data=db('price')
			->alias('s')
			->where(['wx_id'=>1])
			->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function zan()
	{
		$data=db('price')
			->alias('s')
			->where(['wx_id'=>2])
			->select();
		$this->assign('data',$data);
		return $this->fetch('conf');
	}

	public function fensi()
	{
		$data=db('price')
			->alias('s')
			->where(['wx_id'=>3])
			->select();
		$this->assign('data',$data);
		return $this->fetch('conf');
	}

	public function other()
	{
		$data=db('price')
			->alias('s')
			->where(['p_id'=>['<>',10],'wx_id'=>0])
			->select();
		$this->assign('data',$data);
		return $this->fetch('conf');
	}
	public function del()
	{
		$id=input('id');
		$rs=db('price')
			->where(['p_id'=>$id])
			->delete();
		if($rs)
		{
			return ['code'=>1,'msg'=>'删除成功'];
		}
		else
		{
			return ['code'=>0,'msg'=>'删除失败'];
		}
	}
    public function jin()
    {
        $id=input('id');
        $rs=db('price')
            ->where(['p_id'=>$id])
            ->update(["p_power"=>0]);
        if($rs)
        {
            return ['code'=>1,'msg'=>'禁用成功'];
        }
        else
        {
            return ['code'=>0,'msg'=>'禁用失败'];
        }
    }
    public function fang()
    {
        $id=input('id');
        $rs=db('price')
            ->where(['p_id'=>$id])
            ->update(["p_power"=>1]);
        if($rs)
        {
            return ['code'=>1,'msg'=>'允许使用成功'];
        }
        else
        {
            return ['code'=>0,'msg'=>'允许使用失败'];
        }
    }

	public function change()
	{
		if(request()->isPost())
		{
			$id=input('post.id');
			$data['p_name']=input('moshi');
			$data['p_number']=input('num');
			$data['p_price_one']=input('price');
			$data['p_shuoming']=input('desc');
			$data['p_min']=input('low');
			$data['p_max']=input('high');
			$rs=db('price')->where(['p_id'=>$id])->update($data);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$id= input('id');
		$data=db('price')->where(['p_id'=>$id])->find();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function add()
	{
		if(request()->isPost())
		{
			$data['p_name']=input('moshi');
			$data['p_number']=input('num');
			$data['p_price_one']=input('price');
			$data['p_shuoming']=input('desc');
			$data['p_min']=input('low');
			$data['p_max']=input('high');
			$ms = input('ms');
			if($ms=="阅读"){
			    $data["wx_id"]=1;
			    $data["b_id"]=0;
			    $data["api_id"]="阅读2";
			    $data["p_number_danwei"]="阅读量";
            }elseif ($ms=="点赞"){
                $data["wx_id"]=2;
                $data["b_id"]=0;
            }elseif ($ms=="粉丝"){
                $data["wx_id"]=3;
                $data["b_id"]=0;
            }elseif ($ms==1){
                $data["wx_id"]=0;
                $data["b_id"]=1;
            }
            elseif ($ms==2){
                $data["wx_id"]=0;
                $data["b_id"]=2;
            }
            elseif ($ms==3){
                $data["wx_id"]=0;
                $data["b_id"]=3;
            }
            elseif ($ms==4){
                $data["wx_id"]=0;
                $data["b_id"]=4;
            }
			$data["p_power"] = 1;
			$rs=db('price')->insert($data);
			if($rs)
			{
			    if($ms=="阅读"){
                    return ['code'=>0,'msg'=>'添加成功','ms'=>"read"];
                }
			    if ($ms=="点赞"){
                    return ['code'=>0,'msg'=>'添加成功','ms'=>"zan"];
                }
			    if($ms=="粉丝"){
                    return ['code'=>0,'msg'=>'添加成功','ms'=>"fensi"];
                }
			}
			else
			{
				return ['code'=>0,'msg'=>'添加失败'];
			}
		}
		$ls =db("business")->select();
		//var_dump($ls);
		$this->assign("ls",$ls);
		return $this->fetch();
	}
	public function shou(){
        $id=input('id');
        $rs=db('price')
            ->where(['p_id'=>$id])
            ->update(["api_id"=>"阅读2"]);
        if($rs)
        {
            return ['code'=>1,'msg'=>'修改成功'];
        }
        else
        {
            return ['code'=>0,'msg'=>'修改失败'];
        }
    }
	public function zi(){
        $id=input('id');
        $rs=db('price')
            ->where(['p_id'=>$id])
            ->update(["api_id"=>"阅读1"]);
        if($rs)
        {
            return ['code'=>1,'msg'=>'修改成功'];
        }
        else
        {
            return ['code'=>0,'msg'=>'修改失败'];
        }
    }
}