<?php
namespace app\shua_admin\controller;

use think\Controller;
class Admin extends Controller
{
	public function conf()
	{
		if(request()->isPost())
		{
			$where['name']=['like','%'.input('post.key').'%'];
		}
		$where['id']=['<>',0];
		$data=db('admin')
			->field('id,name,case power when 1 then "超级管理员" when 2 then "管理员" end as power')
			->where($where)
			->paginate(20);
		$arr=$data->toArray();
		$this->assign('page',$data->render());
		$this->assign('data',$arr['data']);
		$this->assign('num',$arr['total']);
		return $this->fetch();
	}

	public function add()
	{
		if(request()->isPost())
		{
			$data['name']=input('post.username');
			$data['pwd']=input('post.pwd');
			$data['power']=input('post.power');
			//用户名不能为空
			if(!$data['name'])
			{
				return ['code'=>0,'msg'=>'请填写用户名'];
			}
			//单用户名只能注册一个
			$a=db('admin')->where(['name'=>$data['name']])->find();
			if($a)
			{
				return ['code'=>0,'msg'=>'此用户已被注册'];
			}
			//密码长度必须大于六位
			if(strlen($data['pwd'])<6)
			{
				return ['code'=>0,'msg'=>'密码长度必须大于六位'];
			}
			$rs=db('admin')->insert($data);
			if($rs)
			{
				return ['code'=>1,'msg'=>'添加成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'添加失败'];
			}
		}
		return $this->fetch();
	}

	public function edit()
	{
		if(request()->isPost())
		{
			$id=input('post.id');
			$data['name']=input('post.username');
			$data['power']=input('post.power');
			$pwd=input('post.pwd');
			$cpwd=0;
			if($pwd)
			{
				$data['pwd']=MD5($pwd);
				$cpwd=1;
			}
			if(!$id)
			{
				return ['code'=>0,'msg'=>'参数有误！'];
			}
			$da=db('admin')->where(['id'=>$id])->find();
			//id为1的用户权限不能修改
			if($id==1)
			{
				if($data['power']!=$da['power'])
				{
					return ['code'=>0,'msg'=>'此用户的权限不允许被修改'];
				}
				if(0)//$id!=session('admin_id'))
				{
					return ['code'=>0,'msg'=>'您没有权限修改此用户的信息'];
				}
			}
			$rs=db('admin')->where(['id'=>$id])->update($data);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$id=input('get.id');
		if(!$id)
		{
			header("status:404 not found");
			echo "<center><h1>404 NOT FOUND</h1><center>";
			die();
		}
		$data=db('admin')->where(['id'=>$id])->find();
		if(!$data)
		{
			header("status:404 not found");
			echo "<center><h1>404 NOT FOUND</h1><center>";
			die();
		}
		else
		{
			$this->assign('data',$data);
		}
		return $this->fetch();
	}

	public function del()
	{
		if(request()->isPost())
		{
			$id=input('post.id');
			if($id==1)
			{
				return ['code'=>0,'msg'=>'此用户不能删除'];
			}
			$rs=db('admin')->where(['id'=>$id])->delete();
			if($rs)
			{
				return ['code'=>1,'msg'=>'删除成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'删除失败'];
			}
		}
	}
}