<?php
namespace app\shua_admin\controller;

use think\Controller;
use think\Db;

class Money extends Controller
{
	public function conf ()
	{
		adminLogin();
		$where=session('moneyconf');
		//print_r($where);die;
		if(request()->isPost())
		{
			if(input('post.start')||input('post.end'))
			{
				if(input('post.start'))
				{
					$start=strtotime(input('post.start'));
				}
				else
				{
					$start=0;
				}
				if(input('post.end'))
				{
					$end=strtotime(input('post.end'));
				}
				else
				{
					$end=9999999999;
				}
				$where['o.po_time']=['between',"$start,$end"];
			}

				$where['u.u_phone']=['like','%'.input('post.key').'%'];

		}
		$where['o.po_id']=['<>',0];
		session('moneyconf',$where);
		$data=db('price_order')
			->alias('o')
			->join('user u','u.u_id=o.u_id')
			->where($where)
			->field('
					o.po_id as id,
					o.po_name as name,
					o.po_val as money,
					from_unixtime(o.po_time,"%Y-%m-%d %H:%i:%s") as time ,
					u.u_phone as acc,
					o.po_name as type,
					o.po_price_before as bef,
					o.po_price_after as lef
					')
            ->order("o.po_time DESC")
			->paginate(15);
		$dt=$data->toArray();
		$this->assign('data',$dt['data']);
		$this->assign('num',$dt['total']);
		$this->assign('page',$data->render());
		return $this->fetch();
	}

	public function export()
	{
		adminLogin();
		if(!session('moneyconf'))
		{
			$where['o.po_id']=['<>',0];
		}
		else
		{
			$where=session('moneyconf');
		}
		$data=db('price_order')
			->alias('o')
			->join('user u','u.u_id=o.u_id')
			->where($where)
			->field('
					o.po_id as id,
					o.po_name as name,
					o.po_val as money,
					from_unixtime(o.po_time,"%Y-%m-%d %H:%i:%s") as time ,
					u.u_phone as acc,
					o.po_name as type,
					o.po_price_after as lef
					')
			->select();
		//引入类库
		vendor('excel.PHPExcel');
		vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

		//实例化类库
		$excel= new \PHPExcel();
		$titles=[
		"A1"=>'编号',
		'B1'=>'账号',
		'C1'=>'类型',
		'D1'=>'时间',
		'E1'=>'金额',
		'F1'=>'余额'
		];
		//设置头部
		foreach($titles as $k=>$v)
		{
			$excel->getActiveSheet(0)->setCellValue($k,$v);
		}

		//数据主体
		$row=2;
		for($i=0;$i<count($data);$i++)
		{
			$excel->getActiveSheet(0)->setCellValue('A'.$row,$data[$i]['id']);
			$excel->getActiveSheet(0)->setCellValue('B'.$row,$data[$i]['acc']);
			$excel->getActiveSheet(0)->setCellValue('C'.$row,$data[$i]['type']);
			$excel->getActiveSheet(0)->setCellValue('D'.$row,$data[$i]['time']);
			$excel->getActiveSheet(0)->setCellValue('E'.$row,$data[$i]['money']);
			$excel->getActiveSheet(0)->setCellValue('F'.$row,$data[$i]['lef']);
			$row++;
		}

		$objWriter = new \PHPExcel_Writer_Excel2007($excel);

		//设置响应头
		header("Content-Type:application/force-download");
		//设置文件名
		header('Content-Disposition:attachment;filename="'.date('Ymd').'.xlsx"');

		//输出二进制文件
		$objWriter->save('php://output');
	}
}
?>