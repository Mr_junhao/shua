<?php

namespace app\shua_admin\controller;

use think\Controller;
use think\Session;

class qita extends Controller
{
    public function qita_order(){
        adminLogin();
		$where=session('qiwhere');
        $id = Session::get('admin_id');
        $where['qita_id']=['<>',0];
        if (request()->isPost()) {
            $where['qita_name']= ['like','%'.input('key').'%'];
			$type=input('type');
			if($type!==null&&$type!='')
			{
				$where['qita_type']=$type;
				if($type=='none')
				{
					unset($where['qita_type']);
				}
			}
        }
		if(input('pagesize'))
		{
			session('pagesize',input('pagesize'));
			$pagesize=input('pagesize');
		}
		else
		{
			if(session('pagesize'))
			{
				$pagesize=session('pagesize');
			}
			else
			{
				$pagesize=10;
				session('pagesize',10);
			}
			
		}
		session('qiwhere',$where);
        $rs = db("qita_order")
            ->alias('q')
            ->join('shua_user u','q.u_id = u.u_id')
            ->where($where)
            ->order("qita_time_start DESC")
            ->field('
				u.u_phone,
				u.u_money,
				q.qita_leixing,
				q.qita_name,
				q.qita_number_plan,
				q.qita_moshi,
				q.qita_id,
				q.qita_color,
				q.qita_type,
				from_unixtime(qita_time_start,"%Y-%m-%d %H:%m:%s") as qita_time_start,
				q.qita_price,
				from_unixtime(qita_time_end,"%Y-%m-%d %H:%m:%s") as qita_time_end
				
				')
            ->paginate($pagesize);
        $db =db("qita_order")->alias('f')
            ->select();
        $count = count($db);
        Session::set("qita_count",$count);
        $page = $rs->render();
        $this->assign('length', $rs->toArray()['total']);
        $sx = db("system")->where(["name"=>"shuaxin"])->find();
        $shuaxin =$sx["val"]*1000;
        return view('qita_order', ['rs' => $rs, 'page' => $page,"shuaxin"=>$shuaxin,'pagesize'=>$pagesize]);
    }

	public function exp()
	{
		$where=session('qiwhere');
		$data = db("qita_order")
            ->alias('q')
            ->join('shua_user u','q.u_id = u.u_id')
            ->where($where)
            ->order("qita_time_start DESC")
            ->field('
				u.u_name,
				u.u_money,
				q.qita_leixing,
				q.qita_name,
				q.qita_number_plan,
				q.qita_moshi,
				q.qita_id
				q.qita_type,
				from_unixtime(qita_time_start,"%Y-%m-%d %H:%m:%s") as qita_time_start,
				q.qita_price,
				from_unixtime(qita_time_end,"%Y-%m-%d %H:%m:%s") as qita_time_end				
				')
            ->select();
		//引入类库
		vendor('excel.PHPExcel');
		vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

		//实例化类库
		$excel= new \PHPExcel();
		$titles=[
		"A1"=>'下单用户',
		'B1'=>'余额',
		'C1'=>'业务类型',
		'D1'=>'链接',
		'E1'=>'计划量',
		'F1'=>'模式',
		'G1'=>'状态',
		'H1'=>'下单时间',
		'I1'=>'消费金额',
		'J1'=>'完成时间'
		];
		//设置头部
		foreach($titles as $k=>$v)
		{
			$excel->getActiveSheet(0)->setCellValue($k,$v);
		}

		//数据主体
		$row=2;
		for($i=0;$i<count($data);$i++)
		{
			$type=$data[$i]['qita_type']==0?"待执行":$data[$i]['qita_type']==1?"执行中":"已撤单";

			$excel->getActiveSheet(0)->setCellValue('A'.$row,$data[$i]['u_name']);
			$excel->getActiveSheet(0)->setCellValue('B'.$row,$data[$i]['u_money']);
			$excel->getActiveSheet(0)->setCellValue('C'.$row,$data[$i]['qita_leixing']);
			$excel->getActiveSheet(0)->setCellValue('D'.$row,$data[$i]['qita_name']);
			$excel->getActiveSheet(0)->setCellValue('E'.$row,$data[$i]['qita_number_plan']);
			$excel->getActiveSheet(0)->setCellValue('F'.$row,$data[$i]['qita_moshi']);
			$excel->getActiveSheet(0)->setCellValue('G'.$row,$type);
			$excel->getActiveSheet(0)->setCellValue('H'.$row,$data[$i]['qita_time_start']);
			$excel->getActiveSheet(0)->setCellValue('I'.$row,$data[$i]['qita_price'].'元');
			$excel->getActiveSheet(0)->setCellValue('J'.$row,$data[$i]['qita_time_end']);
			$row++;
		}

		$objWriter = new \PHPExcel_Writer_Excel2007($excel);
		//设置响应头
		header("Content-Type:application/force-download");
		//设置文件名
		header('Content-Disposition:attachment;filename="'.date('Ymd').'.xlsx"');

		//输出二进制文件
		$objWriter->save('php://output');
	}
    public function zhixing(){
        adminLogin();
        if(request()->isPost()){
            $qita_id = input("key");
            $upd_qita = db("qita_order")->where(["qita_id"=>$qita_id])->update(["qita_type"=>1]);
            if(!$upd_qita) {
                $this->error('执行失败');
            } else {
                $this->success("执行成功");
            }

        }
    }
    public function read(){

    }
    public function jiange(){
        adminLogin();
        if (request()->isPost()) {
            $miao =input("miao");
            $sys_upd =db("system")->where(["name"=>"shuaxin"])->update(["val"=>$miao]);
            if(!$sys_upd){
                $this->error("刷新间隔调整失败！");
            }else{
                $this->success("刷新间隔调整成功！");
            }
        }
    }
    public function qita_dingdan(){
        $count_before = Session::get("qita_count");
        $list = db("qita_order")->select();
        $count_after = count($list);
        Session::set("qita_count",$count_after);
        if($count_after>$count_before){
            return ['code'=>1,'msg'=>'综合业务有新订单'];
        }
    }
    public function qita_zhongzhi(){
        adminLogin();
        $qita_id = input("zz");
        $upd = db("qita_order")->where(["qita_id"=>$qita_id])->update(["qita_type"=>4,"qita_time_end"=>time()]);
        if ($upd){
            return ['code'=>1,'msg'=>'订单终止成功！'];
        }
        if(!$upd){
            return ['code'=>0,'msg'=>'订单终止失败！'];
        }
    }
    public function qita_finish(){
        adminLogin();
        $qita_id = input("zz");
        $upd = db("qita_order")->where(["qita_id"=>$qita_id])->update(["qita_type"=>3,"qita_time_end"=>time()]);
        if ($upd){
            return ['code'=>1,'msg'=>'订单修改成功！'];
        }
        if(!$upd){
            return ['code'=>0,'msg'=>'订单修改失败！'];
        }
    }
    public function qita_color(){
        $qita_id = input("id");
        $qita_color = input("color");
        //var_dump(input());die;
        db("qita_order")->where(["qita_id"=>$qita_id])->update(["qita_color"=>$qita_color]);
    }
}