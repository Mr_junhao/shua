<?php
namespace app\shua_admin\controller;

use think\Controller;
use think\Db;
use think\Session;

class Priceinfo extends Controller
{
	//显示
	public function wx()
	{
		adminLogin();
		$data=db('price')->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function info()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$name=db('price')->where(['p_id'=>$id])->value('p_shuoming');
			return ['msg'=>$name];
		}
	}

	public function change()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$val=input('post.val');
			$change=db('price')->where(['p_id'=>$id])->update(['p_shuoming'=>$val]);
			if($change)
			{
				return ['msg'=>'修改成功'];
			}
			else
			{
				return ['msg'=>'修改失败'];
			}
		}
	}

	public function edit()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$data['p_number']=input('post.num');
			$data['p_price_one']=input('post.price');
			$data['p_shuoming']=input('post.shuoming');
			//需要id
			if(!$id)
			{
				return ['code'=>0,'msg'=>'参数有误'];
			}
			if(!db('price')->where(['p_id'=>$id])->find())
			{
				return ['code'=>0,'msg'=>'没有该订单'];
			}
			try
			{
				Db::startTrans();
				$res=db('price')->where(['p_id'=>$id])->update($data);
				if($res)
				{
					Db::commit();
					return ['code'=>1,'msg'=>'修改成功'];
				}
				else
				{
					Db::rollback();
					return ['code'=>0,'msg'=>'修改失败'];
				}
			}
			catch(\Exception $e)
			{
				Db::rollback();
				return ['code'=>0,'msg'=>'错误代码:e077'];
			}
		}
		$id=input()['id'];
		$data=db('price')->where(['p_id'=>$id])->find();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function message()
	{
		adminLogin();
			adminLogin();
		if(request()->isPost())
		{
			$data=input('post.data');
			$rs=db('info')->where(['id'=>1])->update(['val'=>$data]);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
			
		}
		$message=db('info')->where(['id'=>1])->value('val');
		$this->assign('msg',$message);
		return $this->fetch();
	}

	public function msginfo()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$data=db('info')->where(['id'=>$id])->find();
			if(!$data||!$id)
			{
				return ['code'=>0,'msg'=>'参数有误'];
			}
			$rs=db('info')->where(['id'=>$id])->update(['val'=>input('post.val')]);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$id=input('id');
		$data=db('info')->where(['id'=>$id])->find();
		$this->assign('data',$data);
		return $this->fetch();
	}
}
?>