<?php

namespace app\shua_admin\controller;

use think\Controller;
use think\Session;
use think\Url;

class order extends Controller
{
    public function order()
    {
        adminLogin();
        $id = Session::get('admin_id');
        $where = session('rowheres');
        $where['r.ro_id'] = ['<>', 0];
        $pagesize = 10;
        if (input('pagesize')) {
            session('pagesize', input('pagesize'));
            $pagesize = input('pagesize');
        } else {
            if (session('pagesize')) {
                $pagesize = session('pagesize');
            } else {
                $pagesize = 10;
                session('pagesize', 10);
            }

        }
        if (request()->isPost()) {
            $s = input('key');
            $miao = input('miao');
            $where['r.ro_url|r.ro_name'] = ['like', '%' . input('key') . '%'];
            $type = input('type');
            if ($type !== null && $type != '') {
                $where['ro_type'] = $type;
                if ($type == 'none') {
                    unset($where['ro_type']);
                }
            }
        }
        session('rowheres', $where);
        $db = db("read_order")->alias('r')
            ->join('shua_user u', 'r.u_id = u.u_id')
            ->where($where)
            ->order("r.ro_time_start DESC")
            ->select();
        $db2 = db("read_order")->select();
        $count = count($db2);
        Session::set("read_count", $count);
        for ($i = 0; $i < count($db); $i++) {
            if ($db[$i]["api_id"] == "阅读1" && $db[$i]["ro_type"] == 0) {
                $url = "http://112.74.169.43:8104/api/user_define";
                $post = [
                    "links" => [$db[$i]["ro_url"]]
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                $return = curl_exec($ch);
                curl_close($ch);
                //var_dump(json_encode($post));
                $aaa = db("read_order")->where(["ro_id" => $db[$i]["ro_id"]])->update(["ro_type" => 1]);
                if (!$aaa) {
                    $this->error("阅读量自动执行失败！");
                    continue;
                }
            }
        }
        $rs = db("read_order")
            ->alias('r')
            ->join('shua_user u', 'r.u_id = u.u_id')
            ->where($where)
            ->order("r.ro_time_start DESC")
            ->field("
					r.ro_id,
					u.u_phone,
					u.u_money,
					r.ro_name,
					r.ro_number_start,
					r.ro_number_plan,
					r.ro_number,
					r.ro_type,
					r.ro_moshi,
					from_unixtime(r.ro_time_start,'%Y-%m-%d %H:%i:%s') as ro_time_start,
					r.ro_price,
					from_unixtime(r.ro_time_end,'%Y-%m-%d %H:%i:%s') as ro_time_end,
					r.ro_url,
                    r.ro_color
				")
            ->paginate($pagesize);


        $page = $rs->render();
        $this->assign('pagesize', $pagesize);
        $this->assign('length', $rs->toArray()['total']);
        $sx = db("system")->where(["name" => "shuaxin"])->find();
        $shuaxin = (int)($sx["val"]) * 1000;
        return view('order', ['rs' => $rs, 'page' => $page, "shuaxin" => $shuaxin]);
    }

    public function exportorder()
    {
        adminLogin();
        if (session('rowheres')) {
            $where = session('rowheres');
        } else {
            $where['shua_read_order.ro_id'] = ['<>', 0];
        }
        //查询数据
        $data = db("read_order")
            ->alias('r')
            ->join('shua_user u', 'r.u_id = u.u_id')
            ->where($where)
            ->order("r.ro_time_start DESC")
            ->field("
					r.ro_id,
					u.u_name,
					u.u_money,
					r.ro_name,
					r.ro_number_start,
					r.ro_number_plan,
					r.ro_number,
					r.ro_type,
					r.ro_moshi,
					from_unixtime(r.ro_time_start,'%Y-%m-%d %H:%i:%s') as ro_time_start,
					r.ro_price,
					from_unixtime(r.ro_time_end,'%Y-%m-%d %H:%i:%s') as ro_time_end,
					r.ro_url

				")
            ->select();
        //引入类库
        vendor('excel.PHPExcel');
        vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

        //实例化类库
        $excel = new \PHPExcel();
        $titles = [
            "A1" => '下单用户',
            'B1' => '余额',
            'C1' => '文章',
            'D1' => '初始量',
            'E1' => '计划量',
            'F1' => '当前量',
            'G1' => '完成比例',
            'H1' => '模式',
            'I1' => '状态',
            'J1' => '下单时间',
            'K1' => '消费金额',
            'L1' => '完成时间',
            'M1' => '文章链接'
        ];
        //设置头部
        foreach ($titles as $k => $v) {
            $excel->getActiveSheet(0)->setCellValue($k, $v);
        }

        //数据主体
        $row = 2;
        for ($i = 0; $i < count($data); $i++) {
            $type = $data[$i]['ro_type'] == 0 ? "待执行" : $data[$i]['ro_type'] == 1 ? "执行中" : "已撤单";
            $sunum = ($data[$i]['ro_number'] / $data[$i]['ro_number_plan'] * 100 - $data[$i]['ro_number_start'] / $data[$i]['ro_number_plan'] * 100);
            $sunum = (string)sprintf("%.2f", $sunum) . '%';

            $excel->getActiveSheet(0)->setCellValue('A' . $row, $data[$i]['u_name']);
            $excel->getActiveSheet(0)->setCellValue('B' . $row, $data[$i]['u_money']);
            $excel->getActiveSheet(0)->setCellValue('C' . $row, $data[$i]['ro_name']);
            $excel->getActiveSheet(0)->setCellValue('D' . $row, $data[$i]['ro_number_start']);
            $excel->getActiveSheet(0)->setCellValue('E' . $row, $data[$i]['ro_number_plan']);
            $excel->getActiveSheet(0)->setCellValue('F' . $row, $data[$i]['ro_number']);
            $excel->getActiveSheet(0)->setCellValue('G' . $row, $sunum);
            $excel->getActiveSheet(0)->setCellValue('H' . $row, $data[$i]['ro_moshi']);
            $excel->getActiveSheet(0)->setCellValue('I' . $row, $type);
            $excel->getActiveSheet(0)->setCellValue('J' . $row, $data[$i]['ro_time_start']);
            $excel->getActiveSheet(0)->setCellValue('K' . $row, $data[$i]['ro_price']);
            $excel->getActiveSheet(0)->setCellValue('L' . $row, $data[$i]['ro_time_end']);
            $excel->getActiveSheet(0)->setCellValue('M' . $row, $data[$i]['ro_url']);
            $row++;
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($excel);

        //设置响应头
        header("Content-Type:application/force-download");
        //设置文件名
        header('Content-Disposition:attachment;filename="' . date('Ymd') . '.xlsx"');

        //输出二进制文件
        $objWriter->save('php://output');
    }

    public function changetime()
    {
        if (request()->isPost()) {
            $miao = input("miao");
            if ($miao) {
                $sys_upd = db("system")->where(["name" => "shuaxin"])->update(["val" => $miao]);
                if (!$sys_upd) {
                    $this->error("刷新间隔调整失败！");
                } else {
                    $this->success("刷新间隔调整成功！");
                }
            }
        }
    }

    public function zhixing()
    {
        adminLogin();
        if (request()->isPost()) {
            $ro_id = input("key");
            //var_dump($ro_id);die;
            $ls = db("read_order")->where(["ro_id" => $ro_id])->find();
            if ($ls["api_id"] != "阅读1" && $ls["ro_type"] == 0) {
                $url = "http://112.74.169.43:8104/api/user_define";
                $post = [
                    "links" => [$ls["ro_url"]]
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                $return = curl_exec($ch);
                curl_close($ch);
                //var_dump($return);die;
                $aaa = db("read_order")->where(["ro_id" => $ls["ro_id"]])->update(["ro_type" => 1]);
                if (!$aaa) {
                    $this->error("阅读量手动执行失败！");
                }
            }
            /* $upd_ro = db("read_order")->where(["ro_id"=>$ro_id])->update($upd);
             if(!$upd_ro) {
                 $this->error('执行失败');
             } else {
                 $this->success("执行成功");
             }*/
        }
        //return view('order');

    }

    public function zan_order()
    {
        adminLogin();
        //curl 爬取网页 获取文章标题与阅读量  价格比例
        $id = Session::get('id');
        $where = session('zanorderx');

        $where['z.zo_id'] = ['<>', 0];
        if (request()->isPost()) {
            $s = input("key");
            $where['z.zo_url|z.zo_name'] = ['like', '%' . input('post.key') . '%'];
            $type = input('type');
            if ($type !== null && $type != '') {
                $where['zo_type'] = $type;
                if ($type == 'none') {
                    unset($where['zo_type']);
                }
            }
        }
        $pagesize = 10;
        if (input('pagesize')) {
            session('pagesize', input('pagesize'));
        } else {
            if (session('pagesize')) {
                $pagesize = session('pagesize');
            } else {
                $page = 10;
                session('pagesize', 10);
            }

        }
        session('zanorderx', $where);
        $db = db("zan_order")->alias('z')
            ->join('shua_user u', 'z.u_id = u.u_id')
            ->where($where)
            ->order("z.zo_time_start DESC")
            ->select();
        $db2 = db("zan_order")->select();
        $count = count($db2);
        Session::set("zan_count", $count);
        for ($i = 0; $i < count($db); $i++) {
            if ($db[$i]["api_id"] == "点赞1" && $db[$i]["zo_type"] == 0) {
                $url = "http://112.74.169.43:8104/api/user_define";
                //var_dump(json_encode($post));
                $url3 = $db[$i]["zo_url"];
                $urll = urlencode($url3);
                //var_dump($api_id);die;
                $zo_number_plan = $db[$i]["zo_number_plan"];
                //var_dump(1111);die;
                $post["url"] = $urll;
                $post['number'] = $zo_number_plan;
                $post["speed_model"] = 0;
                $post['from'] = "wechat";
                $post["token"] = "d7b3129f5ea0e39f7d70eaca716fcf51";
                //$api_url = "http://wechaa.goudaoke.com/api/home//read/create";
                $api_url = "http://wechaa.goudaoke.com/api/home//fabulous/create";
                $res = $this->post_curls($api_url, $post);
                $result = json_decode($res, true);
                $return_message = (array)$result;
                //var_dump($res);die;
                $message = $result["message"];
                if ($message == "微信阅读点赞今日已满,暂停下单") {
                    echo("http://wechaa.goudaoke.com/api/home//fabulous/create,此api微信阅读点赞今日已满,暂停下单");
                    continue;
                }
                if ($message == "抱歉，您的余额不足请充值后再试") {
                    echo "http://wechaa.goudaoke.com/api/home//fabulous/create,此api余额不足！请充值后再试！";
                    continue;
                }

                //var_dump($message);die;
                $orderid = $message["orderid"];
                //var_dump($orderid);die;
                $url = "http://wechaa.goudaoke.com/api/home//read/get_orderid";
                $post["token"] = "d7b3129f5ea0e39f7d70eaca716fcf51";
                $post["orderid"] = $orderid;
                $res = $this->post_curls($url, $post);
                /*echo "<pre>";
                var_dump(json_decode($res));die;*/
                // sleep("5");
                $resultt = json_decode($res);
                $ress = (array)$resultt;
                $message = $ress["message"];
                $like_num = $message->init_count;
                /*echo "<pre>";
                var_dump($message);die;*/
                $upd['zo_number_start'] = $like_num;
                $upd['orderid'] = $orderid;
                $upd["zo_type"] = 1;
                $upd_zo = db("zan_order")->where(["zo_id" => $db[$i]["zo_id"]])->update($upd);
                if (!$upd_zo) {
                    $this->error('执行失败');
                }
            }
            if ($db[$i]["api_id"] != "点赞1" && $db[$i]["zo_type"] == 0) {
                $url = "http://112.74.169.43:8104/api/user_define";
                $post = [
                    "links" => [$db[$i]["zo_url"]]
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                $return = curl_exec($ch);
                curl_close($ch);
                $return_message = (array)json_decode($return);
                $message = $return_message["message"];
                if ($message != "链接已入库") {
                    echo("http://112.74.169.43:8104/api/user_define 此api连接未入库");
                    continue;
                }
                $upd["zo_type"] = 1;
                $upd_zo = db("zan_order")->where(["zo_id" => $db[$i]["zo_id"]])->update($upd);
                if (!$upd_zo) {
                    $this->error('执行失败');
                }
            }

        }
        $rs = db("zan_order")
            ->alias('z')
            ->join('shua_user u', 'z.u_id = u.u_id')
            ->field("
				u.u_phone,
				z.zo_id,
				u.u_money,
				z.zo_name,
				z.zo_number_start,
				z.zo_number_plan,
				z.zo_number,
				z.zo_type,
				z.zo_moshi,
				from_unixtime(z.zo_time_start,'%Y-%m-%d %H:%m:%s') as zo_time_start,
				z.zo_price,
				from_unixtime(z.zo_time_end,'%Y-%m-%d %H:%m:%s') as zo_time_end,
				z.zo_url,
				z.api_id,
				z.zo_color
			")
            ->where($where)
            ->order("z.zo_time_start DESC")
            ->paginate($pagesize);


        $page = $rs->render();
        $this->assign('length', $rs->toArray()['total']);
        $sx = db("system")->where(["name" => "shuaxin"])->find();
        $shuaxin = (int)($sx["val"]) * 1000;
        return view('zan_order', ['rs' => $rs, 'page' => $page, "shuaxin" => $shuaxin, 'pagesize' => $pagesize]);
    }

    public function zanexp()
    {
        adminLogin();
        $where = session('zanorderx');
        $data = db("zan_order")
            ->alias('z')
            ->join('shua_user u', 'z.u_id = u.u_id')
            ->field("
				u.u_name,
				z.zo_id,
				u.u_money,
				z.zo_name,
				z.zo_number_start,
				z.zo_number_plan,
				z.zo_number,
				z.zo_type,
				z.zo_moshi,
				from_unixtime(z.zo_time_start,'%Y-%m-%d %H:%m:%s') as zo_time_start,
				z.zo_price,
				from_unixtime(z.zo_time_end,'%Y-%m-%d %H:%m:%s') as zo_time_end,
				z.zo_url,
				z.api_id
			")
            ->where($where)
            ->order("z.zo_time_start DESC")
            ->select();

        //引入类库
        vendor('excel.PHPExcel');
        vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

        //实例化类库
        $excel = new \PHPExcel();
        $titles = [
            "A1" => '下单用户',
            'B1' => '余额',
            'C1' => '文章',
            'D1' => '初始量',
            'E1' => '计划量',
            'F1' => '当前量',
            'G1' => '完成比例',
            'H1' => '模式',
            'I1' => '状态',
            'J1' => '下单时间',
            'K1' => '消费金额',
            'L1' => '完成时间',
            'M1' => '文章链接'
        ];
        //设置头部
        foreach ($titles as $k => $v) {
            $excel->getActiveSheet(0)->setCellValue($k, $v);
        }

        //数据主体
        $row = 2;
        for ($i = 0; $i < count($data); $i++) {
            $type = $data[$i]['zo_type'] == 0 ? "待执行" : $data[$i]['zo_type'] == 1 ? "执行中" : $data[$i]['zo_type'] == 2 ? "已撤单" : "已完成";
            $sunum = ($data[$i]['zo_number'] / $data[$i]['zo_number_plan'] * 100 - $data[$i]['zo_number_start'] / $data[$i]['zo_number_plan'] * 100);
            $sunum = (string)sprintf("%.2f", $sunum) . '%';

            $excel->getActiveSheet(0)->setCellValue('A' . $row, $data[$i]['u_name']);
            $excel->getActiveSheet(0)->setCellValue('B' . $row, $data[$i]['u_money']);
            $excel->getActiveSheet(0)->setCellValue('C' . $row, $data[$i]['zo_name']);
            $excel->getActiveSheet(0)->setCellValue('D' . $row, $data[$i]['zo_number_start']);
            $excel->getActiveSheet(0)->setCellValue('E' . $row, $data[$i]['zo_number_plan']);
            $excel->getActiveSheet(0)->setCellValue('F' . $row, $data[$i]['zo_number']);
            $excel->getActiveSheet(0)->setCellValue('G' . $row, $sunum);
            $excel->getActiveSheet(0)->setCellValue('H' . $row, $data[$i]['zo_moshi']);
            $excel->getActiveSheet(0)->setCellValue('I' . $row, $type);
            $excel->getActiveSheet(0)->setCellValue('J' . $row, $data[$i]['zo_time_start']);
            $excel->getActiveSheet(0)->setCellValue('K' . $row, $data[$i]['zo_price']);
            $excel->getActiveSheet(0)->setCellValue('L' . $row, $data[$i]['zo_time_end']);
            $excel->getActiveSheet(0)->setCellValue('M' . $row, $data[$i]['zo_url']);
            $row++;
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($excel);

        //设置响应头
        header("Content-Type:application/force-download");
        //设置文件名
        header('Content-Disposition:attachment;filename="' . date('Ymd') . '.xlsx"');

        //输出二进制文件
        $objWriter->save('php://output');
    }

    public function zan_zhixing()
    {
        adminLogin();
        $id = Session::get('id');
        if (request()->isPost()) {
            $zo_id = input("key");
            //var_dump($zo_id);die;
            $ls = db("zan_order")->where(["zo_id" => $zo_id])->find();
            //var_dump($ls);die;
            $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
            $urll = urlencode($url3);
            $api_id = $ls["api_id"];
            if ($api_id != "点赞1" && $ls["zo_type"] == 0) {
                $url = "http://112.74.169.43:8104/api/user_define";
                $post = [
                    "links" => [$ls["zo_url"]]
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                $return = curl_exec($ch);
                curl_close($ch);
                //var_dump(json_encode($post));
                //var_dump($return);die;
                $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
                $urll = urlencode($url3);
                if ($ls["zo_number_start"] == null) {
                    // var_dump($url3);
                    $url = "http://112.74.169.43:8104/api/user_define/get_by_link";
                    $post = [
                        "link" => "$url3"
                    ];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                    $return = curl_exec($ch);
                    //var_dump($return);die;
                    curl_close($ch);
                    $return5 = json_decode($return, true);
                    $zzz = (array)$return5;
                    //var_dump($return);die;
                    sleep("4");
                    if ($return == '无此链接对应的文章') {
                        $this->error("未获取初始，请稍后。。。", "zan_order");
                    } else {
                        $return_data = $zzz["data"];
                        $like_num = $return_data["likeNum"];
                    }
                    $dbss = db("zan_order")->where(["zo_id" => $ls["zo_id"]])->update(["zo_number_start" => $like_num]);
                    $upd["zo_type"] = 1;
                    $upd["zo_number"]= $like_num;
                    $upd_zo = db("zan_order")->where(["zo_id" => $zo_id])->update($upd);
                    if (!$upd_zo) {
                        $this->error('执行失败');
                    }
                }
                $url_del = "http://112.74.169.43:8104/api/user_define/delete_by_link";
                $post_del = [
                    "link" => "$url3"
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url_del);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_del));// 必须为字符串
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                $return2 = curl_exec($ch);

            } else {
            }

        }
    }

    public function fen_order()
    {
        adminLogin();
        $id = Session::get('id');
        $where = session('feno');
        $where['f.fen_id'] = ['<>', 0];
        if (input('pagesize')) {
            session('pagesize', input('pagesize'));
            $pagesize = input('pagesize');
        } else {
            if (session('pagesize')) {
                $pagesize = session('pagesize');
            } else {
                $pagesize = 10;
                session('pagesize', 10);
            }

        }
        if (request()->isPost()) {
            $s = input("key");
            $where['f.fen_name|f.fen_name'] = ['like', '%' . input('key') . '%'];
            $type = input('type');
            if ($type !== null && $type != '') {
                $where['fen_type'] = $type;
                if ($type == 'none') {
                    unset($where['fen_type']);
                }
            }
        }
        session('feno', $where);
        $rs = db("fen_order")
            ->alias('f')
            ->join('shua_user u', 'f.u_id = u.u_id')
            ->field("
				f.fen_id,
				u.u_phone,
				u.u_money,
				f.fen_name,
				f.fen_number_plan,
				f.fen_moshi,
				f.fen_type,
				f.schedule,
				f.api_id,
				from_unixtime(f.fen_time_start,'%Y-%m-%d %H:%i:%s') as fen_time_start,
				f.fen_price,
				from_unixtime(f.fen_time_end,'%Y-%m-%d %H:%i:%s') as fen_time_end
				")
            ->where($where)
            ->order("fen_time_start DESC")
            ->paginate($pagesize);
        $db = db("fen_order")->alias('f')
            ->join('shua_user u', 'f.u_id = u.u_id')
            ->where($where)
            ->order("f.fen_time_start DESC")
            ->select();
        $db2 = db("fen_order")->select();
        $count = count($db2);
        Session::set("fen_count", $count);
        for ($i = 0; $i < count($db); $i++) {
            if ($db[$i]["api_id"] == "粉丝1" && $db[$i]["fen_type"] == 0) {
                $post["wechat"] = $db[$i]["fen_name"];
                $post["title"] = $db[$i]["fen_name"];
                $post["number"] = $db[$i]["fen_number_plan"];
                $post["speed"] = 0;
                $post["token"] = "d7b3129f5ea0e39f7d70eaca716fcf51";
                $api_url = "http://wechaa.goudaoke.com/api/home//follow/create";
                $res = $this->post_curls($api_url, $post);
                //var_dump($res);die;
                $result = json_decode($res);
                //var_dump($result);die;
                $message = $result->message;
                if ($message == "抱歉，暂停下单") {
                    echo("http://wechaa.goudaoke.com/api/home//fabulous/create,此api微信粉丝暂停下单");
                    continue;
                }
                if ($message == "抱歉，您的余额不足请充值后再试") {
                    echo "http://wechaa.goudaoke.com/api/home//fabulous/create,此api余额不足！请充值后再试！";
                    continue;
                }
                $orderid = $message->orderid;
                //var_dump($orderid);die;
                $upd['orderid'] = $orderid;
                $upd["fen_type"] = 1;
                $upd_fen = db("fen_order")->where(["fen_id" => $db[$i]["fen_id"]])->update($upd);
                if (!$upd_fen) {
                    $this->error('执行失败');
                }
                //else {
//                    ("执行成功");
//                }
            } elseif ($db[$i]["api_id"] == "粉丝2" && $db[$i]["fen_type"] == 0) {
                $post2["token"] = "6909204319c6c5e3213656bae923679b";
                $post2["maxNum"] = $db[$i]["fen_number_plan"];
                $post2["account"] = $db[$i]["fen_name"];
                $api_url2 = "http://www.lol666.com.cn/index.php/Home/fensiapi/add";
                $res = $this->post_curls($api_url2, $post2);
                //var_dump($res);die;
                $result = json_decode($res);
                //var_dump($result);die;
                $message = $result->result;
                //var_dump($message);die;
                $orderid = $message->id;
                //var_dump($orderid);die;
                $upd['orderid'] = $orderid;
                $upd["fen_type"] = 1;
                $upd_fen = db("fen_order")->where(["fen_id" => $db[$i]["fen_id"]])->update($upd);
                if (!$upd_fen) {
                    $this->error('执行失败');
                }
//                else {
//                    echo("执行成功");
//                }
            }
        }
        $length = db("fen_order")->alias('f')->where($where)->count('fen_id');
        $page = $rs->render();
        $this->assign('length', $length);
        $sx = db("system")->where(["name" => "shuaxin"])->find();
        $shuaxin = $sx["val"] * 1000;
        return view('fen_order', ['rs' => $rs, 'page' => $page, "shuaxin" => $shuaxin, 'pagesize' => $pagesize]);
    }

    public function fenexp()
    {
        adminLogin();
        $where = session('feno');
        $data = db("fen_order")
            ->alias('f')
            ->join('shua_user u', 'f.u_id = u.u_id')
            ->field("
				f.fen_id,
				u.u_name,
				u.u_money,
				f.fen_name,
				f.fen_number_plan,
				f.fen_moshi,
				f.fen_type,
				from_unixtime(f.fen_time_start,'%Y-%m-%d %H:%i:%s') as fen_time_start,
				f.fen_price,
				from_unixtime(f.fen_time_end,'%Y-%m-%d %H:%i:%s') as fen_time_end
				")
            ->where($where)
            ->order("fen_time_start DESC")
            ->select();


        //引入类库
        vendor('excel.PHPExcel');
        vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

        //实例化类库
        $excel = new \PHPExcel();
        $titles = [
            "A1" => '下单用户',
            'B1' => '余额',
            'C1' => '微信号/ID',
            'D1' => '计划粉丝量',
            'E1' => '模式',
            'F1' => '状态',
            'G1' => '下单时间',
            'H1' => '消费金额',
            'I1' => '完成时间'
        ];
        //设置头部
        foreach ($titles as $k => $v) {
            $excel->getActiveSheet(0)->setCellValue($k, $v);
        }

        //数据主体
        $row = 2;
        for ($i = 0; $i < count($data); $i++) {
            $type = $data[$i]['fen_type'] == 0 ? "待执行" : $data[$i]['fen_type'] == 1 ? "执行中" : $data[$i]['fen_type'] == 2 ? "已撤单" : "已完成";

            $excel->getActiveSheet(0)->setCellValue('A' . $row, $data[$i]['u_name']);
            $excel->getActiveSheet(0)->setCellValue('B' . $row, $data[$i]['u_money']);
            $excel->getActiveSheet(0)->setCellValue('C' . $row, $data[$i]['fen_name']);
            $excel->getActiveSheet(0)->setCellValue('D' . $row, $data[$i]['fen_number_plan']);
            $excel->getActiveSheet(0)->setCellValue('E' . $row, $data[$i]['fen_moshi']);
            $excel->getActiveSheet(0)->setCellValue('F' . $row, $type);
            $excel->getActiveSheet(0)->setCellValue('G' . $row, $data[$i]['fen_time_start']);
            $excel->getActiveSheet(0)->setCellValue('H' . $row, $data[$i]['fen_price'] . '元');
            $excel->getActiveSheet(0)->setCellValue('I' . $row, $data[$i]['fen_time_end']);
            $row++;
        }

        $objWriter = new \PHPExcel_Writer_Excel2007($excel);

        //设置响应头
        header("Content-Type:application/force-download");
        //设置文件名
        header('Content-Disposition:attachment;filename="' . date('Ymd') . '.xlsx"');

        //输出二进制文件
        $objWriter->save('php://output');
    }

    public function fen_zhixing()
    {
        adminLogin();
        $id = Session::get('id');
        if (request()->isPost()) {
            $fen_id = input("key");
            //var_dump($zo_id);die;
            $ls = db("fen_order")->where(["fen_id" => $fen_id])->find();
            $api_id = $ls["api_id"];
            $fen_number_plan = $ls["fen_number_plan"];
            if ($api_id != "粉丝1" && $api_id != "粉丝2") {
                $upd["fen_type"] = 1;
                $upd_fen = db("fen_order")->where(["fen_id" => $fen_id])->update($upd);
                if (!$upd_fen) {
                    $this->error('执行失败');
                } else {
                    echo("执行成功");
                }
            }
        }
    }

    public function post_curls($url, $post)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        //sleep(5);
        $res = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno' . curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $res; // 返回数据，json格式

    }

    public function read()
    {
        adminLogin();
        $vals = input();
        if ($vals) {
            $val = (array)$vals["vals"];
            for ($i = 0; $i < count($vals["vals"]); $i++) {
                $ls = db("read_order")->where(['ro_id' => $val[$i]])->find();
                $data[$i] = $ls["ro_number"];
                //echo $data[$i];die;
                if ($ls["ro_number_start"] == null && $ls["ro_type"] == 1) {
                    $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
                    // var_dump($url3);
                    $url = "http://112.74.169.43:8104/api/user_define/get_by_link";
                    $post = [
                        "link" => "$url3"
                    ];
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                    $return = curl_exec($ch);
                    //var_dump($return);
                    curl_close($ch);
                    if ($return == "无此链接对应的文章") {
                        $this->inner($url3);
                        continue;
                    }
                    //var_dump(json_decode($return) );die;
                    //$return2 = json_encode($return);
                    $return5 = json_decode($return, true);
                    $zzz = (array)$return5;
                    //var_dump($zzz);die;
                    $return_data = $zzz["data"];
                    $read_num = $return_data["readNum"];
                    //var_dump(json_decode($return) );die;
                    $data[$i] = "go(0)";
                    db("read_order")->where(['ro_id' => $val[$i]])->update(["ro_number_start" => $read_num]);
                } elseif ($ls["ro_type"] == 1 && $ls["ro_number_start"] != null) {
                    $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
                    // var_dump($url3);
                    $url = "http://112.74.169.43:8104/api/user_define/get_by_link";
                    $post = [
                        "link" => "$url3"
                    ];
                    $ch1 = curl_init();
                    curl_setopt($ch1, CURLOPT_URL, $url);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch1, CURLOPT_POST, 1);
                    curl_setopt($ch1, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                    curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                    $return1 = curl_exec($ch1);
                    //var_dump($return1);
                    curl_close($ch1);
                    $return2 = json_decode($return1);
                    //  var_dump($return1);echo "<br />";echo "<br />";echo"<br />";
                    if ($return1 == "无此链接对应的文章") {
                        continue;
                    }
                    $data_res = (array)$return2;
                    //var_dump($return1);die;
                    $read_num_res = $data_res["data"];
                    $read_num = $read_num_res->readNum;
                    //var_dump($read_num);

                    $data[$i] = $read_num;
                    db("read_order")->where(['ro_id' => $val[$i]])->update(["ro_number" => $read_num]);
                    if ($read_num >= ($ls["ro_number_start"] + $ls["ro_number_plan"]) || $read_num >= 100001) {
                        db("read_order")->where(['ro_id' => $val[$i]])->update(["ro_type" => 3, "ro_time_end" => time()]);
                    }
                    if (($ls["ro_number"] - $ls["ro_number_start"]) / $ls["ro_number_plan"] >= 1) {
                        $url_del = "http://112.74.169.43:8104/api/user_define/delete_by_link";
                        $post_del = [
                            "link" => "$url3"
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url_del);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_del));// 必须为字符串
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                        $return2 = curl_exec($ch);
                    }
                }
                //var_dump($data[$i]);die;
                //return $data[$i];
            }
            return $data;
        }
    }

    public function zan()
    {
        adminLogin();
        $vals = input();
        if ($vals) {
            $val = $vals["vals"];
            $count = count($vals["vals"]);
            //var_dump($count);
            for ($i = 0; $i < $count; $i++) {
                $ls = db("zan_order")->where(['zo_id' => $val[$i]])->find();
                //var_dump($ls);die;
                $data[$i] = $ls["zo_number"];
                //echo"<pre>";
                // var_dump($ls);die;
                if ($ls["api_id"] == "点赞1") {
                    if ($ls["zo_type"] == 0 || $ls["zo_type"] == 2) {
                        $data[$i] = 0;
                    } elseif ($ls["zo_type"] == 1) {
                        //var_dump($ls["orderid"]);
                        if ($ls["orderid"] != null || $ls["orderid"] != '') {
                            $url = "http://wechaa.goudaoke.com/api/home//read/get_orderid";
                            $post["token"] = "d7b3129f5ea0e39f7d70eaca716fcf51";
                            $post["orderid"] = $ls["orderid"];
                            //var_dump($post["orderid"]);die;
                            $res = $this->post_curls($url, $post);
                            //var_dump(json_decode($res));die;
                            $abc = (array)json_decode($res);
                            $message = (array)$abc["message"];
                            //print_r($message);die;
                            // die;
                            $parse_count = $message["parse_count"];
                            //var_dump($parse_count);

                            $count = $message["count"];
                            //var_dump($parse_count."+".$count);die;
                            $init_count2 = $message["init_count"];
                            db("zan_order")->where(["zo_id" => $val[$i]])->update(["zo_number_start" => $init_count2]);
                            $zo_number22 = $init_count2 + $parse_count;
                            $time = time();
                            if ($parse_count >= $count) {
                                db("zan_order")->where(['zo_id' => $val[$i]])->update(["zo_type" => 3, "zo_number" => $zo_number22, "zo_time_end" => $time]);
                                $data[$i] = $ls["zo_number_start"] + $parse_count;
                            } else {
                                $zo_number = $ls["zo_number_start"] + $parse_count;
                                db("zan_order")->where(['zo_id' => $val[$i]])->update(["zo_number" => $zo_number]);
                                $data[$i] = $zo_number;
                            }
                        } else {
                            $data[$i] = $ls["zo_number"];
                        }
                    }
                }
                if ($ls["api_id"] != "点赞1") {
                    if ($ls["zo_type"] == 0 || $ls["zo_type"] == 2) {
                        $data[$i] = 0;
                    }
                    if ($ls["zo_number_start"] == null && $ls["zo_type"] == 1) {
                        $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
                        // var_dump($url3);
                        $url = "http://112.74.169.43:8104/api/user_define/get_by_link";
                        $post = [
                            "link" => "$url3"
                        ];
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                        $return = curl_exec($ch);
                        //var_dump($return);
                        curl_close($ch);
                        if ($return == "无此链接对应的文章") {
                            $this->inner($url3);
                            continue;

                        }
                        //var_dump(json_decode($return) );die;
                        //$return2 = json_encode($return);
                        $return5 = json_decode($return, true);
                        $zzz = (array)$return5;
                        //var_dump($zzz);die;
                        $return_data = $zzz["data"];
                        $likeNum = $return_data["likeNum"];
                        //var_dump(json_decode($return) );die;
                        $data[$i] = "go(0)";
                        db("zan_order")->where(['zo_id' => $val[$i]])->update(["zo_number_start" => $likeNum]);
                    } else {
                        $url3 = "https://mp.weixin.qq.com/s?__biz=" . $ls["biz"] . "&mid=" . $ls["mid"] . "&idx=" . $ls["idx"] . "&sn=" . $ls["sn"];
                        $urll = urlencode($url3);
                        if ($ls["zo_type"] == 0 || $ls["zo_type"] == 2) {
                            $data[$i] = 0;
                        } elseif ($ls["zo_type"] == 1) {
                            // var_dump($url3);
                            $url4 = "http://112.74.169.43:8104/api/user_define/get_by_link";
                            $post = [
                                "link" => "$url3"
                            ];
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url4);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_POST, 1);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                            $return = curl_exec($ch);
                            curl_close($ch);
                            if ($return == "无此链接对应的文章") {
                               continue;
                            }
                            //var_dump(json_decode($return) );die;
                            //$return2 = json_encode($return);
                            $return5 = json_decode($return, true);
                            $zzz = (array)$return5;
                            $return_data = $zzz["data"];
                            $like_num = $return_data["likeNum"];
                            //var_dump($like_num );die;
                            $data[$i] = $like_num;
                            db("zan_order")->where(['zo_id' => $val[$i]])->update(["zo_number" => $like_num]);
                            if ($like_num >= ($ls["zo_number_start"] + $ls["zo_number_plan"])) {
                                db("zan_order")->where(['zo_id' => $val[$i]])->update(["zo_type" => 3, "zo_time_end" => time()]);
                                $url = "http://112.74.169.43:8104/api/user_define/delete_by_link";
                                $post = [
                                    "link" => "$url3"
                                ];
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POST, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
                                $return = curl_exec($ch);
                                curl_close($ch);
                            }
                        } else {
                            $data[$i] = $ls["zo_number"];
                        }
                    }
                }
            }
            return $data;
            //echo json_encode($data);
        }
    }

    public function fen()
    {
        adminLogin();
        $vals = input();
        if ($vals) {
            $val = $vals["vals"];
            for ($i = 0; $i < count($vals["vals"]); $i++) {
                $ls = db("fen_order")->where(['fen_id' => $val[$i]])->find();
                $data[$i] = $ls["schedule"] . "/" . $ls["fen_number_plan"];
                if ($ls["api_id"] == "粉丝1" && $ls["fen_type"] == 1) {
                    $url = "http://wechaa.goudaoke.com/api/home//follow/get_orderid";
                    $post["token"] = "d7b3129f5ea0e39f7d70eaca716fcf51";
                    $post["orderid"] = $ls["orderid"];
                    $res = $this->post_curls($url, $post);
                    $result = json_decode($res)->message;
                    //$body = $result->body;
                    // $aaa= json_encode($body);
                    //$bb=json_decode($result);
                    $parse_count = $result->parse_count;
                    $count = $result->count;
                    //var_dump($count);die;
                    $schedule = $parse_count;
                    db("fen_order")->where(["fen_id" => $val[$i]])->update(["schedule" => $schedule]);
                    $data[$i] = $parse_count . "/" . $ls["fen_number_plan"];
                    $time = time();
                    if ($parse_count >= $count) {
                        $data[$i] = $count . "/" . $ls["fen_number_plan"];
                        db("fen_order")->where(['fen_id' => $val[$i]])->update(["fen_type" => 3, "fen_time_end" => $time]);
                    }
                }
                if ($ls["api_id"] == "粉丝2" && $ls["fen_type"] == 1) {
                    $url = "http://www.lol666.com.cn/index.php/Home/fensiapi/order_list";
                    $post["token"] = "6909204319c6c5e3213656bae923679b";
                    $post["ids"] = $ls["orderid"];
                    $res = $this->post_curls($url, $post);
                    //var_dump(json_decode($res));die;
                    $assss = json_decode($res);
                    // print_r($assss);die;
                    $result = $assss->result;
                    $resultt = $result[0];
                    //print_r($result);die;
                    //$body = $result->body;
                    // $aaa= json_encode($body);
                    //$bb=json_decode($result);
//                /var_dump($resultt);die;
                    $status = $resultt->status;
                    $currentNum = $resultt->currentnum;
                    $schedule = $currentNum;
                    db("fen_order")->where(["fen_id" => $val[$i]])->update(["schedule" => $schedule]);
                    //var_dump($currentNum);die;
                    $data[$i] = $currentNum . "/" . $ls["fen_number_plan"];
                    // print_r($status);die;
                    $time = time();
                    if ($status == 100) {
                        db("fen_order")->where(['fen_id' => $val[$i]])->update(["fen_type" => 3, "fen_time_end" => $time]);
                    }
                }
            }
            return $data;
        }
    }

    public function zhongzhi(){
        adminLogin();
        $ro_id = input("zz");
        $ls = db("read_order")->where(["ro_id"=>$ro_id])->find();
        $ro_number = $ls["ro_number"];
        $ro_number_start = $ls["ro_number_start"];
        $ro_number_plan = $ls["ro_number_plan"];

        //var_dump($ro_id);die;
        $bili = ($ro_number-$ro_number_start)/$ro_number_plan;
        $ro_price = $ls["ro_price"];
        $f_price1=sprintf("%.2f",substr(sprintf("%.3f", $ro_price*$bili), 0, -1));
        $f_price =$ro_price-sprintf("%.2f",substr(sprintf("%.3f", $ro_price*$bili), 0, -1));
        $u_id = $ls["u_id"];
        $user = db("user")->where(["u_id"=>$u_id])->find();
        $user_price =$user["u_money"];
        $u_money = $f_price+$user_price;
        //var_dump($u_id);die;
        $user_upd = db("user")->where(["u_id"=>$u_id])->update(["u_money"=>$u_money]);
        //var_dump($user_upd);die;
        if (!$user_upd){
            $this->error("业务终止失败！");
        }else{
            $price_order["po_name"]="阅读量业务终止退款";
            $price_order["po_val"]="+".$f_price;
            $price_order["po_time"] = time();
            $price_order["u_id"]=$u_id;
            $price_order["po_price_before"]=$user_price;
            $price_order["po_price_after"] = $u_money;
            $inser_price = db("price_order")->insert($price_order);
            if(!$inser_price){
                $this->error("业务终止失败！");
            }
            $db_read_zz = db("read_order")->where(["ro_id"=>$ro_id])->update(["ro_type"=>4,"ro_time_end"=>time(),"ro_price"=>$f_price1]);

            if(!$db_read_zz){
                $this->error("阅读业务终止失败！");
            }
            $this->success("终止成功");
        }
    }

    public function zan_zhongzhi()
    {
        adminLogin();
        $zo_id = input("zz");
        // var_dump($zo_id);
        $ls = db("zan_order")->where(["zo_id" => $zo_id])->find();
        $zo_number = $ls["zo_number"];
        $zo_number_start = $ls["zo_number_start"];
        $zo_number_plan = $ls["zo_number_plan"];
        $bili = ($zo_number - $zo_number_start) / $zo_number_plan;
        $zo_price = $ls["zo_price"];
        $f_price1 = sprintf("%.2f", substr(sprintf("%.3f", $zo_price * $bili), 0, -1));
        $f_price = $zo_price - sprintf("%.2f", substr(sprintf("%.3f", $zo_price * $bili), 0, -1));
        $u_id = $ls["u_id"];
        $user = db("user")->where(["u_id" => $u_id])->find();
        $user_price = $user["u_money"];
        $u_money = $f_price + $user_price;
        //var_dump($u_id);die;
        $user_upd = db("user")->where(["u_id" => $u_id])->update(["u_money" => $u_money]);
        //var_dump($user_upd);die;
        if (!$user_upd) {
            $this->error("业务终止失败！");
        } else {
            $price_order["po_name"] = "点赞业务终止退款";
            $price_order["po_val"] = "+" . $f_price;
            $price_order["po_time"] = time();
            $price_order["u_id"] = $u_id;
            $price_order["po_price_before"] = $user_price;
            $price_order["po_price_after"] = $u_money;
            $inser_price = db("price_order")->insert($price_order);
            if (!$inser_price) {
                $this->error("业务终止失败！");
            }
            db("zan_order")->where(["zo_id" => $zo_id])->update(["zo_type" => 4, "zo_time_end" => time(), "zo_price" => $f_price1]);
            $this->success("终止成功");
        }
    }

    public function fen_zhongzhi()
    {
        adminLogin();
        $fen_id = input("zz");
        $ls = db("fen_order")->where(["fen_id" => $fen_id])->find();
        $schedule = $ls["schedule"];
        $fen_number_plan = $ls["fen_number_plan"];
        $bili = $schedule / $fen_number_plan;
        $fen_price = $ls["fen_price"];
        $f_price1 = sprintf("%.2f", substr(sprintf("%.3f", $fen_price * $bili), 0, -1));
        $f_price = $fen_price - sprintf("%.2f", substr(sprintf("%.3f", $fen_price * $bili), 0, -1));
        $u_id = $ls["u_id"];
        $user = db("user")->where(["u_id" => $u_id])->find();
        $user_price = $user["u_money"];
        $u_money = $f_price + $user_price;
        //var_dump($u_id);die;
        $user_upd = db("user")->where(["u_id" => $u_id])->update(["u_money" => $u_money]);
        //var_dump($user_upd);die;
        if (!$user_upd) {
            $this->error("业务终止失败！");
        } else {
            $price_order["po_name"] = "粉丝业务终止退款";
            $price_order["po_val"] = "+" . $f_price;
            $price_order["po_time"] = time();
            $price_order["u_id"] = $u_id;
            $price_order["po_price_before"] = $user_price;
            $price_order["po_price_after"] = $u_money;
            $inser_price = db("price_order")->insert($price_order);
            if (!$inser_price) {
                $this->error("业务终止失败！");
            }
            db("fen_order")->where(["fen_id" => $fen_id])->update(["fen_type" => 4, "fen_time_end" => time(), "fen_price" => $f_price1]);
            $this->success("终止成功");
        }
    }

    public function read_dingdan()
    {
        $count_before = Session::get("read_count");
        $list = db("read_order")->select();
        $count_after = count($list);
        Session::set("read_count", $count_after);
        if ($count_after > $count_before) {
            return ['code' => 1, 'msg' => '微信阅读量有新订单'];
        }
    }

    public function zan_dingdan()
    {
        $count_before = Session::get("zan_count");
        $list = db("zan_order")->select();
        $count_after = count($list);
        Session::set("zan_count", $count_after);
        if ($count_after > $count_before) {
            return ['code' => 1, 'msg' => '微信文章赞有新订单'];
        }
    }

    public function fen_dingdan()
    {
        $count_before = Session::get("fen_count");
        $list = db("fen_order")->select();
        $count_after = count($list);
        Session::set("fen_count", $count_after);
        if ($count_after > $count_before) {
            return ['code' => 1, 'msg' => '微信粉丝量有新订单'];
        }
    }

    public function read_color()
    {
        $ro_id = input("id");
        $ro_color = input("color");
        //var_dump(input());die;
        db("read_order")->where(["ro_id" => $ro_id])->update(["ro_color" => $ro_color]);
    }

    public function zan_color()
    {
        $zo_id = input("id");
        $zo_color = input("color");
        //var_dump(input());die;
        db("zan_order")->where(["zo_id" => $zo_id])->update(["zo_color" => $zo_color]);
    }
    public function inner($urlinner){
        $url = "http://112.74.169.43:8104/api/user_define";
        $post = [
            "links" => $urlinner
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
        $return = curl_exec($ch);
        curl_close($ch);
    }
    public function api_inner(){
        $url = "http://112.74.169.43:8104/api/user_define/pre_crawl_count";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $output = curl_exec($ch);
        curl_close($ch);
        $re = json_decode($output, true);
        $return = (array)$re;
        $count = $return["data"]["count"];
        echo( $count);
            //exit(json_encode(array('status'=>-1,'msg'=>'查询成功','count'=>$count)));
    }
    public function api_inner_delete(){
        $url = "http://112.74.169.43:8104/api/user_define/delete_pre_crawl_list";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
       // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
        $return = curl_exec($ch);
        curl_close($ch);
        $re = json_decode($return,true);
        $res = (array)$re;
        /*$message = $res["message"];
        echo($message);*/
        exit(json_encode($res));
    }
    public function mongo_number(){
        $url = "http://112.74.169.43:8104/api/user_define/post_count";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $output = curl_exec($ch);
        curl_close($ch);
        $re = json_decode($output, true);
        $return = (array)$re;
        $count = $return["data"]["count"];
        echo( $count);
    }
    public function mongo_number_delete(){
        $url = "http://112.74.169.43:8104/api/user_define/delete_posts";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
        $return = curl_exec($ch);
        curl_close($ch);
        $re = json_decode($return,true);
        $res = (array)$re;
        /*$message = $res["message"];
        echo($message);*/
        exit(json_encode($res));
    }
    public function inner_mongo(){
        $urls = input("urls");
        $new_arr = explode("\n",$urls);
        $url = "http://112.74.169.43:8104/api/user_define";
        $post = [
            "links" => $new_arr
        ];
       // print_r(json_encode($post));
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));// 必须为字符串
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));// 必须声明请求头
        $return = curl_exec($ch);
        curl_close($ch);
        //var_dump( $return);die;
        if($return =="请传入正确的链接"){
            exit(json_encode(array('state'=>0,'message'=>'请传入正确的链接')));
        }
        exit($return);
    }

}