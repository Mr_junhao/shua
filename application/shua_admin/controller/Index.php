<?php
namespace app\shua_admin\controller;
use think\Controller;
use think\Session;
class Index extends Controller
{
    //显示
    public function index()
    {
        adminLogin();
		return view('index');
    }
	//登录
	public function login()
	{
	    session_start();
	   //var_dump($_SESSION["helloweba_num"]);
	   //var_dump(Session::get("helloweba_num"));
        if(Session::get("count_admin")==""){
            Session::set("count_admin",0);
        }
        $aaaa=Session::get("count_admin");
	    if($_GET){
            $count = input("cu");
            Session::set("count_admin",$count);
        }
		if($_POST)
		{
		    $u_pwd = input("u_pwd");
			$u_phone=input('u_phone');
            $codes = input("codes");
                if ($u_phone == "") {
                    return $this->error("请输入账号！");
                } else {
                    if ($u_pwd == "") {
                        return $this->error("请输入密码！", "login");
                    } else {
                        if($codes!=''){
                            if($codes!=$_SESSION["helloweba_num"]){
                                    return $this->error("验证码不正确！", "login");
                            }
                        }
                        $rs = db('admin')->where(['name' => $u_phone, 'pwd' => md5($u_pwd)])
                            ->find();
                        if ($rs) {
                            Session::set('name', $u_phone);
                            Session::set('admin_id', $rs['id']);
							$ip=request()->ip();
							try
							{
								$log_data['ip']=$ip;
								$log_data['name']=$u_phone;
								$log_data['power']=$rs['power'];
								$log_data['time']=time();
								db('admin_ip')->insert($log_data);
							}
							catch(\Exception $e)
							{
								//nothing to do or add a errlog
								throw(new \Exception($e));
							}
                            return $this->success('登陆成功', 'index');
                        } else {
                            return $this->error("账号或密码不正确");
                        }
                    }
                }
		}
        return view('login',["aaaa"=>$aaaa]);
	}

    function getCode($num, $w, $h)
    {
        $code = "";
        for ($i = 0; $i < $num; $i++) {
            $code .= rand(0, 9);
        }
        $_SESSION["helloweba_num"] = $code;
        \think\Session::set("helloweba_num", $code);
        header("Content-type: image/PNG");
        $im = imagecreate($w, $h);
        $black = imagecolorallocate($im, 0, 0, 0);
        $gray = imagecolorallocate($im, 200, 200, 200);
        $bgcolor = imagecolorallocate($im, 255, 255, 255);
        imagefill($im, 0, 0, $gray);
        imagerectangle($im, 0, 0, $w - 1, $h - 1, $black);
        $style = array($black, $black, $black, $black, $black,
            $gray, $gray, $gray, $gray, $gray
        );
        imagesetstyle($im, $style);
        $y1 = rand(0, $h);
        $y2 = rand(0, $h);
        $y3 = rand(0, $h);
        $y4 = rand(0, $h);
        imageline($im, 0, $y1, $w, $y3, IMG_COLOR_STYLED);
        imageline($im, 0, $y2, $w, $y4, IMG_COLOR_STYLED);
        for ($i = 0; $i < 80; $i++) {
            imagesetpixel($im, rand(0, $w), rand(0, $h), $black);
        }
        $strx = rand(3, 8);
        for ($i = 0; $i < $num; $i++) {
            $strpos = rand(1, 6);
            imagestring($im, 5, $strx, $strpos, substr($code, $i, 1), $black);
            $strx += rand(8, 12);
        }
        imagepng($im);//Êä³öÍ¼Æ¬
        imagedestroy($im);//ÊÍ·ÅÍ¼Æ¬ËùÕ¼ÄÚ´æ

    }
	public function zhuce(){
        if(input('sub'))
        {
            $u_phone=input('u_acount');
            $u_name = input("u_name");
            $u_pwd=input('u_pwd');
            $u_rpwd = input("u_rpwd");
            if ($u_phone==""){
                $this->error("请输入账号！","zhuce");
            }else{
                $acount_length = mb_strlen($u_phone,'utf-8');
                if($acount_length<3||$acount_length>8){
                    $this->error("请输入3——8位用户名！");
                }
            }
            if ($u_name==""){
                $this->error("请输入用户名！","zhuce");
            }else{
                $name_length = mb_strlen($u_name,'utf-8');
                if($name_length<3||$name_length>8){
                    $this->error("请输入3-8位用户名！");
                }
            }
            if($u_pwd==""){
                $this->error("请输入密码！","zhuce");
            }else{
                $pud_length = mb_strlen($u_pwd,'utf-8');
                if($pud_length<6){
                    $this->error("请输入至少6位密码！");
                }
                if($u_rpwd==""){
                    $this->error("请输入密码！","zhuce");
                }else{
                    if ($u_rpwd!=$u_pwd){
                        $this->error("两次密码输入不一致","zhuce");
                    }
                }
            }
            $ss = db("user")->where(["u_phone"=>$u_phone])->find();
            if($ss){
                $this->error("此账号已存在！","zhuce");
            }
            $data["u_phone"]= $u_phone;
            $data["u_name"]=$u_name;
            $data["u_pwd"] = md5($u_pwd);
            $rs = db("user")->insert($data);
            if(!$rs){
                $this->error("注册失败！","zhuce");
            }else{
                $this->success("注册成功！请登录！","login");
            }
        }
        return view("zhuce");
    }
	public function quit()
	{
		Session::clear();
		//echo $this->success('退出成功','login');
        $this->redirect("/shua_admin/index/login");

    }
}