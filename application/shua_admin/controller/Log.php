<?php
namespace app\shua_admin\controller;

use think\Controller;

class Log extends Controller
{
	public function iplog()
	{
		adminLogin();
		$rs=db('admin_ip')
			->field('id,name,case power  when 1 then "超级管理员" when 2 then "管理员" end as power,from_unixtime(time,"%Y-%m-%d %H:%i:%s") as time,ip')
			->order('id desc')
			->paginate(20);
		$ra=$rs->toArray();
		/*echo "<pre>";
		print_r($ra);
		die();*/
		$this->assign('rs',$ra['data']);
		$this->assign('page',$rs->render());
		$this->assign('num',$ra['total']);
		return $this->fetch();
	}
}