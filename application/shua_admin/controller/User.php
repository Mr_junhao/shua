<?php
namespace app\shua_admin\controller;

use think\Controller;
use think\Session;
use think\Db;

class User extends Controller
{
	//显示
	public function conf()
	{
		adminLogin();
		$where=session('uwhere');
		if(request()->isPost())
		{
			$where['u_name|u_phone']=['like','%'.input('post.key').'%'];
		}
		$where['u_id']=['<>',0];
		session('uwhere',$where);
		$data=db('user')
			->where($where)
			->field('u_power,u_id,u_name,u_phone,u_money,from_unixtime(addtime,"%Y-%m-%d %H:%i:%s") as time')
            ->order("addtime DESC")
			->paginate(20);
		$rs=$data->toArray();
		$this->assign('page',$data->render());
		$this->assign('num',$rs['total']);
		$this->assign('data',$rs['data']);
		return $this->fetch();
	}
	//导出数据
	public function exp()
	{
		$where=session('uwhere');
		$data=db('user')
			->where($where)
			->field('u_id,u_name,u_phone,u_money,from_unixtime(addtime,"%Y-%m-%d %H:%i:%s") as time')
			->select();
		//引入类库
		vendor('excel.PHPExcel');
		vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

		//实例化类库
		$excel= new \PHPExcel();
		$titles=[
		"A1"=>'编号',
		'B1'=>'用户名',
		'C1'=>'账号',
		'D1'=>'注册时间',
		'E1'=>'余额'
		];
		//设置头部
		foreach($titles as $k=>$v)
		{
			$excel->getActiveSheet(0)->setCellValue($k,$v);
		}

		//数据主体
		$row=2;
		for($i=0;$i<count($data);$i++)
		{
			$excel->getActiveSheet(0)->setCellValue('A'.$row,$data[$i]['u_id']);
			$excel->getActiveSheet(0)->setCellValue('B'.$row,$data[$i]['u_name']);
			$excel->getActiveSheet(0)->setCellValue('C'.$row,$data[$i]['u_phone']);
			$excel->getActiveSheet(0)->setCellValue('D'.$row,$data[$i]['time']);
			$excel->getActiveSheet(0)->setCellValue('E'.$row,$data[$i]['u_money']);
			$row++;
		}

		$objWriter = new \PHPExcel_Writer_Excel2007($excel);

		//设置响应头
		header("Content-Type:application/force-download");
		//设置文件名
		header('Content-Disposition:attachment;filename="'.date('Ymd').'.xlsx"');

		//输出二进制文件
		$objWriter->save('php://output');
	}
	//充值
    public function recharge(){
		adminLogin();
	    if($_POST){
	        $uname=input('post.username');
			$money=input('post.money');
			$u=db('user')->where(['u_phone'=>$uname])->find();
			if(!$u)
			{
				return ['code'=>0,'msg'=>'不存在的用户'];
			}
			if($money<=-2000)
			{
				return ['code'=>0,'msg'=>'请输入正确的金额'];
			}
			try
			{
			    //var_dump(111);
				Db::startTrans();
				//增加用户余额
				//财务记录
				$logdata['po_name']='后台充值';
				$logdata['po_val']='+'.$money.'元';
				$logdata['po_time']=time();
				$logdata['u_id']=$u['u_id'];
				$logdata['po_price_before']=$u['u_money'].'元';
				$logdata['po_price_after']=$u['u_money']+$money.'元';

				$rs=db('user')->where(['u_id'=>$u['u_id']])->setInc('u_money',$money);
				$rsc=db('price_order')->insert($logdata);
				if($rs&&$rsc)
				{
					Db::commit();
					return ['code'=>1,'msg'=>'充值成功'];
				}
				else
				{
					Db::rollback();
					return ['code'=>0,'msg'=>'充值失败'];
				}
			}
			catch(\Exception $e)
			{
				Db::rollback();
				return ['code'=>'0','msg'=>'错误代码：r012'];
			}
        }
		return $this->fetch();
    }

	public function getinfo()
	{
		adminLogin();
		if(request()->isPost())
		{
			$key=input('post.key');
			$data=db('user')
				->where(['u_phone'=>$key])
				->field('u_name as uname,u_phone as acc,u_money as money')
				->find();
			if($data)
			{
				return ['code'=>1,'msg'=>'success','data'=>$data];
			}
			else
			{
				return ['code'=>0,'msg'=>'不存在的用户'];
			}
		}
	}
	//用户添加
    public function add(){
		adminLogin();
        if(request()->isPost())
		{
			$data['u_name']=input('post.username');
			if(!$data['u_name'])
			{
				return ['code'=>0,'msg'=>'请填写用户名'];
			}
			$pwd=input('post.pwd');
			if(strlen($pwd)<6)
			{
				return ['code'=>0,'msg'=>'密码长度不能小于6位'];
			}
			$data['u_pwd']=md5($pwd);
			if(input('post.phone'))
			{
				$data['u_phone']=input('post.phone');
			}
			if(input('money'))
			{
				$data['u_money']=input('post.money');
			}
			$rs=db('user')->where(['u_name'=>$data['u_name']])->find();
			if($rs)
			{
				return ['code'=>0,'msg'=>'该用户已被注册'];
			}
			$data['addtime']=time();
			if(db('user')->insert($data))
			{
				return ['code'=>1,'msg'=>'添加成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'添加失败'];
			}
		}
		return $this->fetch();
    }
	//用户删除
    public function del(){
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			if(!$id)
			{
				return ['code'=>0,'msg'=>'参数有误'];
			}
			$rs=db('user')->where(['u_id'=>$id])->update(["u_power"=>0]);
			if($rs)
			{
				return ['code'=>1,'msg'=>'操作成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'操作失败'];
			}
		}
    }
    public function fang(){
        adminLogin();
        if(request()->isPost())
        {
            $id=input('post.id');
            if(!$id)
            {
                return ['code'=>0,'msg'=>'参数有误'];
            }
            $rs=db('user')->where(['u_id'=>$id])->update(["u_power"=>1]);
            if($rs)
            {
                return ['code'=>1,'msg'=>'操作成功'];
            }
            else
            {
                return ['code'=>0,'msg'=>'操作失败'];
            }
        }
    }

	public function chongzhi()
	{
		adminLogin();
		$where=session('chongzhi');
		if(request()->isPost())
		{
			$where['u.u_name|u_phone']=['like','%'.input('post.key').'%'];
		}
		if(input('post.start')||input('post.end'))
		{
			if(input('post.start'))
			{
				$start=strtotime(input('post.start'));
			}
			else
			{
				$start=0;
			}
			if(input('post.end'))
			{
				$end=strtotime(input('post.end'));
			}
			else
			{
				$end=9999999999;
			}
			$where['ch_time']=['between',"$start,$end"];
		}
		$where['o.ch_id']=['<>',0];
		session('chongzhi',$where);
		$data=db('ch_order')
			->alias('o')
			->join('user u','u.u_id=o.u_id')
			->where($where)
			->field('
					u.u_name,
					u.u_money as money,
					u.u_phone,
					o.ch_id,
					o.ch_price as u_money,
					o.ch_acount as account,
					from_unixtime(o.ch_time,"%Y-%m-%d %H:%i:%s") as ch_time,
					o.ch_type t,
					from_unixtime(o.ch_time_end,"%Y-%m-%d %H:%i:%s") as end
					')
			->order('o.ch_time DESC')
			->paginate(20);
        $db =db("ch_order")
            ->select();
        $count = count($db);
        Session::set("chong_count",$count);
		$rs=$data->toArray();
		$this->assign('page',$data->render());
		$this->assign('num',$rs['total']);
		$this->assign('data',$rs['data']);
		return $this->fetch();
	}
	//充值通过
	public function confirm()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$dat=db('ch_order')->where(['ch_id'=>$id])->find();
			if(!$dat)
			{
				return ['code'=>o,'msg'=>'不存在的订单'];
			}
			if($dat['ch_type']!=0)
			{
				return ['code'=>0,'msg'=>'该订单已经被处理过'];
			}
			try
			{
				Db::startTrans();
				//修改订单状态
				$change=db('ch_order')->where(['ch_id'=>$id])->update(['ch_type'=>1,'ch_time_end'=>time()]);
				$before = db("user")->where(["u_id"=>$dat['u_id']])->find();
				$money_before = $before["u_money"];
				//增加用户余额
				$inuser=db('user')->where(['u_id'=>$dat['u_id']])->setInc('u_money',$dat['ch_price']);
				//财务记录
				$u=db('user')->where(['u_id'=>$dat['u_id']])->find();
				$logdata['po_name']='充值';
				$logdata['po_val']='+'.$dat['ch_price'].'元';
				$logdata['po_time']=time();
				$logdata['u_id']=$dat['u_id'];
				$logdata['po_price_before']=$money_before.'元';
				$logdata['po_price_after']=$money_before+$dat['ch_price'].'元';

				$rs=db('price_order')->insert($logdata);
				if($change&&$inuser)
				{
					Db::commit();
					return ['code'=>1,'msg'=>'操作成功'];
				}
				else
				{
					Db::rollback();
					return ['code'=>0,'msg'=>'操作失败，请稍后再试'];
				}
			}
			catch(\Exception $e)
			{
				Db::rollback();
				//throw(new \Exception($e));
				return ['code'=>0,'msg'=>'错误代码:c023'];
			}
		}
	}
	//充值驳回
	public function refuse()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			$dat=db('ch_order')->where(['ch_id'=>$id])->find();
			if(!$dat)
			{
				return ['code'=>o,'msg'=>'不存在的订单'];
			}
			if($dat['ch_type']!=0)
			{
				return ['code'=>0,'msg'=>'该订单已经被处理过'];
			}
			try
			{
				Db::startTrans();
				//修改订单状态
				$change=db('ch_order')->where(['ch_id'=>$id])->update(['ch_type'=>2]);
				if($change)
				{
					Db::commit();
					return ['code'=>1,'msg'=>'操作成功'];
				}
				else
				{
					Db::rollback();
					return ['code'=>0,'msg'=>'操作失败，请稍后再试'];
				}
			}
			catch(\Exception $e)
			{
				Db::rollback();
				return ['code'=>0,'msg'=>'错误代码:c036'];
			}
		}
	}

	public function message()
	{
		adminLogin();
		$data=db('system')->select();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function msginfo()
	{
		adminLogin();
		if(request()->isPost())
		{
			$id=input('post.id');
			if(!$id)
			{
				return ['code'=>0,'msg'=>'参数有误'];
			}
			if(!db('system')->where(['id'=>$id])->find())
			{
				return ['code'=>0,'msg'=>'参数有误'];
			}
			$rs=db('system')->where(['id'=>$id])->update(['val'=>input('post.val')]);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$id=input('id');
		if(!$id)
		{
			return $this->error('参数有误');
		}
		if(!db('system')->where(['id'=>$id])->find())
		{
			return $this->error('参数有误');
		}
		$data=db('system')->where(['id'=>$id])->find();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function logconf()
	{
		adminLogin();
		if(request()->isPost())
		{
			$open=input('post.open');
			$rs=db('conf_reg')->where(['id'=>1])->update(['open'=>$open]);
			if($rs)
			{
				return ['code'=>0,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$data=db('conf_reg')->where(['id'=>1])->find();
		$this->assign('data',$data);
		return $this->fetch();
	}

	public function export()
	{
		adminLogin();
		if(session('chongzhi'))
		{
			$where=session('chongzhi');
		}
		else
		{
			$where['o.ch_id']=['<>',0];
		}
		//查询数据
		$data=db('ch_order')
			->alias('o')
			->join('user u','u.u_id=o.u_id')
			->where($where)
			->field('
					u.u_name,
					u.u_money as money,
					u.u_phone,
					o.ch_id,
					o.ch_price as u_money,
					o.ch_acount as account,
					from_unixtime(o.ch_time,"%Y-%m-%d %H:%i:%s") as time,
					case o.ch_type when 0 then "未处理" when 2 then "已驳回" when 1 then "已通过" end as t,
					from_unixtime(o.ch_time_end,"%Y-%m-%d %H:%i:%s") as end
					')
			->order('o.ch_type,o.ch_id')
			->select();
		//引入类库
		vendor('excel.PHPExcel');
		vendor('excel.PHPExcel.Writer.PHPExcel_Writer_Excel2007');

		//实例化类库
		$excel= new \PHPExcel();
		$titles=[
		"A1"=>'编号',
		'B1'=>'用户名',
		'C1'=>'账号',
		'D1'=>'充值金额',
		'E1'=>'账户余额',
		'F1'=>'订单编号',
		'G1'=>'状态',
		'H1'=>'提交时间',
		'I1'=>'结束时间'
		];
		//设置头部
		foreach($titles as $k=>$v)
		{
			$excel->getActiveSheet(0)->setCellValue($k,$v);
		}

		//数据主体
		$row=2;
		for($i=0;$i<count($data);$i++)
		{
			$excel->getActiveSheet(0)->setCellValue('A'.$row,$data[$i]['ch_id']);
			$excel->getActiveSheet(0)->setCellValue('B'.$row,$data[$i]['u_name']);
			$excel->getActiveSheet(0)->setCellValue('C'.$row,$data[$i]['u_phone']);
			$excel->getActiveSheet(0)->setCellValue('D'.$row,$data[$i]['u_money']);
			$excel->getActiveSheet(0)->setCellValue('E'.$row,$data[$i]['money']);
			$excel->getActiveSheet(0)->setCellValue('F'.$row,$data[$i]['account']);
			$excel->getActiveSheet(0)->setCellValue('G'.$row,$data[$i]['t']);
			$excel->getActiveSheet(0)->setCellValue('H'.$row,$data[$i]['time']);
			$excel->getActiveSheet(0)->setCellValue('I'.$row,$data[$i]['end']);
			$row++;
		}

		$objWriter = new \PHPExcel_Writer_Excel2007($excel);

		//设置响应头
		header("Content-Type:application/force-download");
		//设置文件名
		header('Content-Disposition:attachment;filename="'.date('Ymd').'.xlsx"');

		//输出二进制文件
		$objWriter->save('php://output');
	}
    public function explain(){
        adminLogin();
        adminLogin();
        if(request()->isPost())
        {
            $data=input('post.data');
            //var_dump($data);
            $rs=db('price')->where(['p_id'=>10])->update(['p_shuoming'=>$data]);
            //var_dump($rs);
            if($rs)
            {
                return ['code'=>1,'msg'=>'修改成功'];
            }
            else
            {
                return ['code'=>0,'msg'=>'修改失败,未更改'];
            }

        }
        $message=db('price')->where(['p_id'=>10])->value('p_shuoming');
        $this->assign('msg',$message);
        return $this->fetch();
    }
	public function cmsg()
	{
		if(request()->isPost())
		{
			$content=input('post.content');
			//var_dump($content);die;
			$rs=db('price')->where(['p_id'=>10])->update(['p_shuoming'=>$content]);
			if($rs)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
		$this->assign('shuoming',db('price')->where(['p_id'=>10])->value('p_shuoming'));
		return $this->fetch();
	}

	public function changewx()
	{
		if(request()->isPost())
		{
			$data=input("post.images");
			$filename=ROOT_PATH."public".DS."static".DS.'images'.DS.'wx.jpg';
			//echo file_get_contents($filename);
			$rs=file_put_contents($filename,base64_decode(substr($data,strpos($data,','))));
			if($rs!==false)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
	}

	public function changezfb()
	{
		if(request()->isPost())
		{
			$data=input("post.images");
			$filename=ROOT_PATH."public".DS."static".DS.'images'.DS.'zfb.jpg';
			//echo file_get_contents($filename);
			$rs=file_put_contents($filename,base64_decode(substr($data,strpos($data,','))));
			if($rs!==false)
			{
				return ['code'=>1,'msg'=>'修改成功'];
			}
			else
			{
				return ['code'=>0,'msg'=>'修改失败'];
			}
		}
	}
	public function chong_dingdan(){
        $count_before = Session::get("chong_count");
        $list = db("ch_order")->select();
        $count_after = count($list);
        Session::set("chong_count",$count_after);
        if($count_after>$count_before){
            return ['code'=>1,'msg'=>'有新充值订单订单'];
        }
    }
    public function password(){
        adminLogin();
        if($_POST){
            $uname=input('post.username');
            $pwd=input('post.pwd');
            $u=db('user')->where(['u_phone'=>$uname])->find();
            if(!$u)
            {
                return ['code'=>0,'msg'=>'不存在的用户'];
            }

                $rs=db('user')->where(['u_id'=>$u['u_id']])->update(['u_pwd'=>md5($pwd)]);

                if($rs)
                {
                    Db::commit();
                    return ['code'=>1,'msg'=>'修改成功'];
                }
                else
                {
                    Db::rollback();
                    return ['code'=>0,'msg'=>'修改失败'];
                }
            }


        return $this->fetch();
    }
}
?>